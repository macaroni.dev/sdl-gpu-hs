let
  pkgs = import ./../nix/pkgs.nix;
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "puhoy";
    src = ./..;
    subDir = "test";
  };
  compiler-nix-name = "ghc8104";
  configureArgs = "-v3";
}
