{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Concurrent (threadDelay)
import Foreign.C.Types
import SDL.Vect
import qualified SDL

main :: IO ()
main = do
  putStrLn "Puhoy there!"
  sdl

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

sdl :: IO ()
sdl = do
  SDL.initialize [SDL.InitVideo]

  window <- SDL.createWindow "SDL Tutorial" SDL.defaultWindow { SDL.windowInitialSize = V2 screenWidth screenHeight }

  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  SDL.getRendererInfo renderer >>= \ri -> putStrLn $ "getRendererInfo = " ++ show ri

  SDL.rendererDrawColor renderer SDL.$= V4 0 255 0 255
  SDL.clear renderer
  SDL.present renderer
  threadDelay 2000000

  SDL.destroyWindow window
  SDL.quit
