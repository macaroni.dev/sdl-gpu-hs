{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Monad (when)
import Control.Concurrent (threadDelay)
import System.Exit (die)
import Foreign.Ptr
import Foreign.C.Types
import SDL.Vect
import qualified SDL
import qualified SDL.GPU.C as GPU
import qualified SDL.Raw.Types as SDLRaw

import Memorable

main :: IO ()
main = do
  putStrLn "Puhoy there!"

  GPU.setDebugLevel GPU.debugLevelMax
  
  screen <- GPU.init 800 600 GPU.defaultInitFlags

  when (screen == nullPtr) $ die "Failed to GPU.init"

  white <- var (SDLRaw.Color 255 255 255 255)

  GPU.clear screen
  GPU.circleFilled screen 200 200 200 (toPtr white)
  GPU.flip screen

  threadDelay 2000000
  GPU.quit
