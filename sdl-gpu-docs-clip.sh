#! /usr/bin/env bash

nix-build --expr '(import ./nix/pkgs.nix).SDL_gpu' -o SDL_gpu-docs-out

echo "$PWD/SDL_gpu-docs-out/docs/html/index.html" | xsel --clipboard
