]- `pkgconfig-depends` isn't working with `SDL_gpu`
  - I think SDL_gpu may not support pkg-config. [Here's a 2016 PR that adds it though.](https://github.com/grimfang4/sdl-gpu/pull/45)
  - Looks like SDL_gpu gets into `$NIX_CFLAGS_COMPILE`:
```
nix-shell:~/work/sdl-gpu-hs]$ echo $NIX_CFLAGS_COMPILE 
-isystem /nix/store/nrhxlhmrnrvzsj43jia7b75gh5yc6gpg-SDL2-2.0.9-dev/include -isystem /nix/store/7bk5jy02iqzrghnjrd04m3rcfhg427f5-libGL-1.0.0-dev/include -isystem /nix/store/g0smhg45z66xyax4ly02qxn5y8i3g27w-libX11-1.6.7-dev/include -isystem /nix/store/gq3s0dknd4w8dip0qk3bm8fll7y2srlm-xorgproto-2018.4/include -isystem /nix/store/3j8ipqjgj5kaq5qs9ykm6fjad3ajvd6d-libxcb-1.13.1-dev/include -isystem /nix/store/ckabs8yglyxy1zy7iyxj93bmb2q1w99b-SDL_gpu-2019-01-24/include -isystem /nix/store/nrhxlhmrnrvzsj43jia7b75gh5yc6gpg-SDL2-2.0.9-dev/include -isystem /nix/store/7bk5jy02iqzrghnjrd04m3rcfhg427f5-libGL-1.0.0-dev/include -isystem /nix/store/g0smhg45z66xyax4ly02qxn5y8i3g27w-libX11-1.6.7-dev/include -isystem /nix/store/gq3s0dknd4w8dip0qk3bm8fll7y2srlm-xorgproto-2018.4/include -isystem /nix/store/3j8ipqjgj5kaq5qs9ykm6fjad3ajvd6d-libxcb-1.13.1-dev/include -isystem /nix/store/ckabs8yglyxy1zy7iyxj93bmb2q1w99b-SDL_gpu-2019-01-24/include
```
  - Maybe I should just bring in `SDL_gpu` as an explicit `nativeBuildInput` instead of having `cabal` find it via `pkg-config`. That might work?

```
$CC ship-demo-copy.c -isystem /nix/store/nrhxlhmrnrvzsj43jia7b75gh5yc6gpg-SDL2-2.0.9-dev/include/SDL2 -isystem /nix/store/ckabs8yglyxy1zy7iyxj93bmb2q1w99b-SDL_gpu-2019-01-24/include/SDL2 -lm -L /nix/store/ckabs8yglyxy1zy7iyxj93bmb2q1w99b-SDL_gpu-2019-01-24/lib/libSDL2_gpu.so -lSDL2_gpu -L /nix/store/nrhxlhmrnrvzsj43jia7b75gh5yc6gpg-SDL2-2.0.9-dev/lib/libSDL2main.a -lSDL2
```
^ this built it!

---
With an overlay that adds a `.pc` file for `SDL_gpu`, `ship-demo-copy.c` now builds with
```
$CC ship-demo-copy.c $(pkg-config --cflags --libs SDL_gpu sdl2) -lm
```
---

Functions & types used by `ship-demo-copy.c`:
* [x] `GPU_Image`
* [x] `GPU_LoadImage`
* [x] `GPU_FreeImage`
* [x] `GPU_Rect`
* [x] `GPU_Target`
* [x] `GPU_BlitRotate`
* [x] `GPU_SetDebugLevel`
* [x] `GPU_DEBUG_LEVEL_MAX`
* [x] `GPU_Init`
* [x] `GPU_DEFAULT_INIT_FLAGS`

--------

Notice the path of the hsc-generated file:
``` /home/armando/work/macaroni.dev/sdl-gpu-hs/dist-newstyle/build/x86_64-linux/ghc-8.6.5/sdl-gpu-0.1.0.0/build/SDL/GPU/C.hs
```

If I run hsc2hs from ghci and output to that file, I should be able to get hsc working in ghci.

--

View/Projection matrices didn't seem to work. Maybe it's due to the Camera being enabled? They don't seem to be compatible.

---
```
/home/armando/work/macaroni.dev/sdl-gpu-hs/dist-newstyle/build/x86_64-linux/ghc-8.6.5/sdl-gpu-0.1.0.0/build/SDL/GPU/C/TargetControls_hsc_make.o:TargetControls_hsc_make.c:function main: error: undefined reference to 'hsc_Sint16'
/home/armando/work/macaroni.dev/sdl-gpu-hs/dist-newstyle/build/x86_64-linux/ghc-8.6.5/sdl-gpu-0.1.0.0/build/SDL/GPU/C/TargetControls_hsc_make.o:TargetControls_hsc_make.c:function main: error: undefined reference to 'hsc_Sint16'
collect2: error: ld returned 1 exit status
```

^ happens when you do #{Sint16} instead of #{type Sint16}. hsc error messages aren't great, but basically it's trying to invoke Sint16 as a function since it's in the first spot.