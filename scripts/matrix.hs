import qualified SDL.GPU.C as GPU
import qualified SDL.Raw.Types as SDL
import Foreign.C.String
import Foreign.Ptr
import Memorable

:{
script :: IO (Ptr GPU.Target, Ptr GPU.Image)
script = do
  t <- GPU.init 800 600 GPU.defaultInitFlags
  star_image <- withCString "star.png" GPU.loadImage
  pure (t, star_image)
:}
