{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{-
FUTURE WORK:
- Auto offset based on data definition
  - Generics, HKD, and a Type Family to do this
  - TH
- Leverage -XLinearTypes for pure offheap data
  - Can we make the Void record fields linear? Which would force users
    to use be exhaustive in construction? (MyRecord %1 -> Ptr MyRecord -> m ()) -> m Ptr MyRecord - you would have to remember to set each phantom field of MyRecord to fully consume it.
-}
module Memorable where

import Data.Kind
import Data.Proxy
import GHC.TypeLits

import Control.Monad (when)
import Control.Monad.Primitive
import Data.Primitive.ByteArray
import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.ForeignPtr
import Foreign.ForeignPtr.Unsafe
import Foreign.C.Types
import Foreign.Storable
import qualified Foreign.Marshal.Utils
import GHC.Records
import GHC.Generics

-- Cannot be instantiated - just a means to give us record accessors
data Mem (offset :: Nat) field

type (:@) field offset = Mem offset field

class MemData a where
  memDataSize      :: Int
  memDataAlignment :: Int

data MemOf a = MemOf

mem :: forall a. MemOf a
mem = MemOf

memi :: forall a k. ((?mem :: MemOf a) => k) -> k
memi k = let ?mem = mem @a in k

instance (HasField x a (Mem offset field)) => HasField x (MemOf a ) (a -> Mem offset field) where
  getField MemOf = getField @x :: a -> Mem offset field

-- TODO: Use defaults instead? This is a lil nasty
instance {-# OVERLAPPABLE #-} Storable a => MemData a where
  memDataSize = sizeOf (undefined :: a)
  memDataAlignment = alignment (undefined :: a)

intVal :: forall offset. KnownNat offset => Int
intVal = fromIntegral (natVal (Proxy :: Proxy offset))

(&->)
  :: forall field x offset
   . KnownNat offset
  => Ptr (x HaskellMem)
  -> (x ForeignMem -> Mem offset field)
  -> Ptr field
struct &->  _ = plusPtr struct (intVal @offset)

(*->)
  :: KnownNat offset
  => Storable field
  => PtrLike ptr
  => ptr (x HaskellMem)
  -> (x ForeignMem -> Mem offset field)
  -> IO field
struct *-> f = withPtr struct $ \p -> peek (p &-> f)

(*->=)
  :: KnownNat offset
  => Storable field
  => PtrLike ptr
  => (x ForeignMem -> Mem offset field)
  -> field
  -> ptr (x HaskellMem)
  -> IO ()
(*->=) f x struct = withPtr struct $ \p -> poke (p &-> f) x

class PtrLike ptr where
  toPtr :: ptr a -> Ptr a
  withPtr :: ptr a -> (Ptr a -> IO r) -> IO r

instance PtrLike Ptr where
  toPtr = id
  withPtr ptra f = f ptra

instance PtrLike ForeignPtr where
  toPtr = unsafeForeignPtrToPtr
  withPtr fptra f = withForeignPtr fptra f

newtype Pinned a = Pinned (MutableByteArray RealWorld)

newPinned :: forall a. MemData a => IO (Pinned a)
newPinned =
  Pinned <$> newAlignedPinnedByteArray
  (memDataSize @a)
  (memDataAlignment @a)

initPinned :: forall a. MemData a => (forall ptr. PtrLike ptr => ptr a -> IO ()) -> IO (Pinned a)
initPinned init = do
  p <- newPinned
  init p
  pure p

instance PtrLike Pinned where
  toPtr (Pinned mab) = castPtr $ mutableByteArrayContents mab
  withPtr (Pinned mab) f = f $ castPtr $ mutableByteArrayContents mab

-- TODO: Combinators to use MemView to peek/poke. Should compose
-- TODO: Compose Structs (both as MemPtrs and as values. For instance, GPU_Renderer has a non-ptr GPU_Rect, but it also has a GPU_Target*)

data Ex1 = Ex1
  { x :: Mem 0 CInt
  , y :: Mem 4 CInt
  }

-- "ACT LIKE C" UTILS

(.=) :: Storable a => PtrLike ptr => ptr a -> a -> IO ()
(.=) ref a = withPtr ref $ \ptra -> poke ptra a
infix 5 .=

(+=) :: Num a => Storable a => PtrLike ptr => ptr a -> a -> IO ()
ref += n = ref %= (+ n)

infix 5 +=

(-=) :: Num a => Storable a => PtrLike ptr => ptr a -> a -> IO ()
ref -= n = ref %= subtract n
infix 5 -=

(%=) :: Num a => Storable a => PtrLike ptr => ptr a -> (a -> a) -> IO ()
ref %= f = withPtr ref $ \ptra -> do
  old <- peek ptra
  poke ptra (f old)
infix 5 %=

withPeek :: Storable a => PtrLike ptr => ptr a -> (a -> IO r) -> IO r
withPeek ref f = withPtr ref $ \ptra -> do
  a <- peek ptra
  f a

var :: MemData a => Storable a => a -> IO (Pinned a)
var a = do
  p <- newPinned
  withPtr p $ \ptr -> poke ptr a
  pure p

varM :: MemData a => Storable a => IO a -> IO (Pinned a)
varM = (>>= var)

val :: Storable a => PtrLike ptr => ptr a -> IO a
val ref = withPtr ref peek

while :: Monad m => m Bool -> m a -> m ()
while check act = do
    b <- check
    when b $ do
      _ <- act
      while check act

toFloat :: Integral a => a -> Float
toFloat = fromIntegral

data MemMode = ForeignMem | HaskellMem

-- TODO: Auto-magic stuff with descending into Ptrs & marshalling Strings
--
-- It might make sense to make this TF an open TC to allow for custom differences
-- (e.g. C enums vs Haskell sums)
type family MemField (mode :: MemMode) (field :: Type) (offset :: Nat) :: Type where
  MemField ForeignMem field offset = field :@ offset
  MemField HaskellMem field offset = field

class GMemStorable hs mem where
  gmempeek :: Ptr a -> IO (hs x)
  gmempoke :: Ptr a -> (hs x) -> IO ()

instance (GMemStorable hsa mema, GMemStorable hsb memb) => GMemStorable (hsa :*: hsb) (mema :*: memb) where
  gmempeek p = (:*:) <$> gmempeek @hsa @mema p <*> gmempeek @hsb @memb p
  gmempoke p (a :*: b) = do
    gmempoke @hsa @mema p a
    gmempoke @hsb @memb p b

instance GMemStorable hs mem => GMemStorable (M1 _1 _2 hs) (M1 _1 _2 mem) where
  gmempeek p = M1 <$> gmempeek @hs @mem p
  gmempoke p (M1 a) = gmempoke @hs @mem p a

instance (KnownNat offset, Storable hs) => GMemStorable (K1 _1 hs) (K1 _1 (Mem offset field)) where
  gmempeek p = K1 <$> peek (plusPtr p $ intVal @offset)
  gmempoke p (K1 a) = poke (plusPtr p (intVal @offset)) a

newtype MemStorable a = MemStorable a

instance
  ( MemData (x HaskellMem)
  , Generic (x ForeignMem)
  , Generic (x HaskellMem)
  , GMemStorable (Rep (x HaskellMem)) (Rep (x ForeignMem))
  ) => Storable (MemStorable (x HaskellMem)) where
  peek = fmap (MemStorable . to) . gmempeek @(Rep (x HaskellMem)) @(Rep (x ForeignMem))
  poke p (MemStorable a) = gmempoke @(Rep (x HaskellMem)) @(Rep (x ForeignMem)) p (from a)
  sizeOf _ = memDataSize @(x HaskellMem)
  alignment _ = memDataAlignment @(x HaskellMem)

withMem :: Storable (x HaskellMem) => x HaskellMem -> (Ptr (x HaskellMem) -> IO b) -> IO b
withMem = Foreign.Marshal.Utils.with

-- | Useful for munging by-value struct adapter bindings
initMem :: Storable a => (Ptr a -> IO ()) -> IO a
initMem i = alloca $ \p -> i p >> peek p
