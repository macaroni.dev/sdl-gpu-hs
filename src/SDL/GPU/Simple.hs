-- | A mid-level wrapper around the raw C bindings.
--
-- This module is meant to be import qualified as @GPU@ so that its names
-- mirror the C functions (which are prefixed with @GPU_@)
--
-- Here are the "rules" I followed:
-- * All functions run in @forall m. MonadIO m =>@ instead of 'IO' for convenience
-- * 'Ptr' types that are actually references (e.g. 'GPU.Image') stay as 'Ptr's
-- * 'Ptr' types that are adapters for by-reference structs inputs / "outvars"
--   are passed/returned as Haskell types by-value.
-- * 'CString's are replaced with 'String' or 'FilePath' as appropriate
-- * 'GPU.Bool's are replaced with 'Prelude.Bool'
module SDL.GPU.Simple
  ( module SDL.GPU.C.Types
  , module SDL.GPU.C.NumericTypes
  , module SDL.GPU.C.Bool
  , module SDL.GPU.Simple.ContextControls
  , module SDL.GPU.Simple.Conversions
  , module SDL.GPU.Simple.ImageControls
  , module SDL.GPU.Simple.Initialization
  , module SDL.GPU.Simple.Logging
  , module SDL.GPU.Simple.Matrix
  , module SDL.GPU.Simple.RendererControls
  , module SDL.GPU.Simple.RendererSetup
  , module SDL.GPU.Simple.Rendering
  , module SDL.GPU.Simple.ShaderInterface
  , module SDL.GPU.Simple.Shapes
  , module SDL.GPU.Simple.SurfaceControls
  , module SDL.GPU.Simple.TargetControls
  , module SDL.GPU.Simple
  , (.|.)
) where

import Data.Bits ((.|.))

import SDL.GPU.C.Types
import SDL.GPU.C.NumericTypes
import SDL.GPU.C.Bool
import SDL.GPU.Simple.ContextControls
import SDL.GPU.Simple.Conversions
import SDL.GPU.Simple.ImageControls
import SDL.GPU.Simple.Initialization
import SDL.GPU.Simple.Logging
import SDL.GPU.Simple.Matrix
import SDL.GPU.Simple.RendererControls
import SDL.GPU.Simple.RendererSetup
import SDL.GPU.Simple.Rendering
import SDL.GPU.Simple.ShaderInterface
import SDL.GPU.Simple.Shapes
import SDL.GPU.Simple.SurfaceControls
import SDL.GPU.Simple.TargetControls

import Foreign.Ptr
import Control.Monad.IO.Class

withMatrix
  :: forall m r
   . MonadIO m
  => Ptr Target
  -> MatrixMode
  -> m r
  -> m r
withMatrix screen mode k = do
  matrixMode screen mode
  pushMatrix
  r <- k
  matrixMode screen mode
  popMatrix
  pure r

withFreshMatrix
  :: forall m r
   . MonadIO m
  => Ptr Target
  -> MatrixMode
  -> m r
  -> m r
withFreshMatrix screen mode k = withMatrix screen mode (loadIdentity >> k)

withAliasImage
  :: forall m r
   . MonadIO m
  => Ptr Image
  -> (Ptr Image -> m r)
  -> m r
withAliasImage img k = do
  a <- createAliasImage img
  r <- k a
  freeImage a
  pure r
