module SDL.GPU.Simple.Logging where

import Data.Word
import Foreign.Ptr
import Foreign.C.String
import Control.Monad.IO.Class

import SDL.GPU.C as GPU

setDebugLevel
  :: forall m
   . MonadIO m
  => GPU.DebugLevelEnum
  -> m ()
setDebugLevel = liftIO . GPU.setDebugLevel

getDebugLevel
  :: forall m
   . MonadIO m
  => m GPU.DebugLevelEnum
getDebugLevel = liftIO GPU.getDebugLevel

-- NOTE - NOT BOUND: varargs GPU_Log* functions
-- NOTE - NOT BOUND: varargs GPU_SetLogCallback
-- NOTE - NOT BOUND: varargs GPU_PushErrorCode

popErrorCode
  :: forall m
   . MonadIO m
  => Ptr GPU.ErrorObject
  -> m ()
popErrorCode = liftIO . GPU.popErrorCode

getErrorString
  :: forall m
   . MonadIO m
  => GPU.ErrorEnum
  -> m CString
getErrorString = liftIO . GPU.getErrorString

setErrorQueueMax
  :: forall m
   . MonadIO m
  => GPU.UnsignedInt
  -> m ()
setErrorQueueMax = liftIO . GPU.setErrorQueueMax
