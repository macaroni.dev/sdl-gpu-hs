module SDL.GPU.Simple.ImageControls where

import Prelude
import Data.Word
import Data.Int
import Foreign.Ptr
import Foreign.Marshal.Utils (with)
import Foreign.C.String
import Foreign.C.Types
import Control.Monad.IO.Class
import Memorable

import qualified SDL.Raw.Types as SDL.Raw
import qualified SDL

import SDL.GPU.C as GPU

createImage
  :: forall m
   . MonadIO m
  => GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> GPU.FormatEnum
  -> m (Ptr GPU.Image)
createImage w h fmt = liftIO $ GPU.createImage w h fmt

createImageUsingTexture
  :: forall m
   . MonadIO m
  => GPU.TextureHandle
  -> Prelude.Bool -- ^ take_ownership
  -> m (Ptr GPU.Image)
createImageUsingTexture t b = liftIO $ GPU.createImageUsingTexture t (GPU.bool b)

loadImage
  :: forall m
   . MonadIO m
  => FilePath
  -> m (Ptr GPU.Image)
loadImage fp = liftIO $ withCString fp $ GPU.loadImage

createAliasImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m (Ptr GPU.Image)
createAliasImage = liftIO . GPU.createAliasImage

copyImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m (Ptr GPU.Image)
copyImage = liftIO . GPU.copyImage

freeImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m ()
freeImage = liftIO . GPU.freeImage

setImageVirtualResolution
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> m ()
setImageVirtualResolution img w h = liftIO $ GPU.setImageVirtualResolution img w h

unsetImageVirtualResolution
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m ()
unsetImageVirtualResolution = liftIO . GPU.unsetImageVirtualResolution

updateImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Rect -- ^ image_rect
  -> SDL.Surface
  -> GPU.Rect -- ^ surface_rect
  -> m ()
updateImage img img_rect (SDL.Surface s _) s_rect =
  liftIO $ with img_rect \pir -> with s_rect \ssr ->
    GPU.updateImage img pir s ssr

-- TODO: updateImageBytes
-- TODO: replaceImage
-- TODO: saveImage
-- TODO: saveImageRW

generateMipmaps
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m ()
generateMipmaps = liftIO . GPU.generateMipmaps

setColor
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Color
  -> m ()
setColor img c = liftIO $ with c $ GPU.setColor img

unsetColor
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m ()
unsetColor = liftIO . GPU.unsetColor

getBlending
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m Prelude.Bool
getBlending = fmap GPU.isTrue . liftIO . GPU.getBlending

setBlending
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Prelude.Bool
  -> m ()
setBlending img b = liftIO $ GPU.setBlending img (GPU.bool b)

setBlendFunction
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.BlendFuncEnum -- ^ source_color
  -> GPU.BlendFuncEnum -- ^ dest_color
  -> GPU.BlendFuncEnum -- ^ source_alpha
  -> GPU.BlendFuncEnum -- ^ dest_alpha
  -> m ()
setBlendFunction img f1 f2 f3 f4 = liftIO $ GPU.setBlendFunction img f1 f2 f3 f4

setBlendEquation
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.BlendEqEnum -- ^ color_equation
  -> GPU.BlendEqEnum -- ^ alpha_equation
  -> m ()
setBlendEquation img e1 e2 = liftIO $ GPU.setBlendEquation img e1 e2

setBlendMode
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.BlendPresetEnum -- ^ mode
  -> m ()
setBlendMode img m = liftIO $ GPU.setBlendMode img m

setImageFilter
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.FilterEnum -- ^ filter
  -> m ()
setImageFilter img f = liftIO $ GPU.setImageFilter img f

setAnchor
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Float -- ^ anchor_x
  -> GPU.Float -- ^ anchor_y
  -> m ()
setAnchor img x y = liftIO $ GPU.setAnchor img x y

getAnchor
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Ptr GPU.Float -- ^ anchor_x
  -> Ptr GPU.Float -- ^ anchor_y
  -> m ()
getAnchor img x y = liftIO $ GPU.getAnchor img x y

getSnapMode
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m GPU.SnapEnum
getSnapMode = liftIO . GPU.getSnapMode

setSnapMode
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.SnapEnum
  -> m ()
setSnapMode img m = liftIO $ GPU.setSnapMode img m

setWrapMode
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.WrapEnum -- ^ wrap_mode_x  
  -> GPU.WrapEnum -- ^ wrap_mode_y
  -> m ()
setWrapMode img x y = liftIO $ GPU.setWrapMode img x y

getTextureHandle
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m GPU.TextureHandle
getTextureHandle = liftIO . GPU.getTextureHandle
