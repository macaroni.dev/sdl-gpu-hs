module SDL.GPU.Simple.ContextControls where

import Prelude
import Data.Word
import Foreign.Ptr
import SDL.GPU.C as GPU
import Control.Monad.IO.Class
import Memorable
import Foreign.Storable
import Foreign.Marshal.Alloc

getContextTarget :: forall m. MonadIO m => m (Ptr GPU.Target)
getContextTarget = liftIO GPU.getContextTarget

-- TODO: newtype WindowID
getWindowTarget :: forall m. MonadIO m => GPU.Uint32 -> m (Ptr GPU.Target)
getWindowTarget = liftIO . GPU.getWindowTarget

createTargetFromWindow :: forall m. MonadIO m => GPU.Uint32 -> m (Ptr GPU.Target)
createTargetFromWindow = liftIO . GPU.createTargetFromWindow

makeCurrent :: forall m. MonadIO m => Ptr GPU.Target -> GPU.Uint32 -> m ()
makeCurrent t w = liftIO $ GPU.makeCurrent t w

setWindowResolution
  :: forall m
   . MonadIO m
  => GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> m Prelude.Bool
setWindowResolution w h = fmap GPU.isTrue $ liftIO $ GPU.setWindowResolution w h

getFullscreen
  :: forall m
   . MonadIO m
  => m Prelude.Bool
getFullscreen = fmap GPU.isTrue $ liftIO $ GPU.getFullscreen

setFullscreen
  :: forall m
   . MonadIO m
  => Prelude.Bool -- ^ enable_fullscreen
  -> Prelude.Bool -- ^ use_desktop_resolution
  -> m Prelude.Bool
setFullscreen ef udr = fmap GPU.isTrue $ liftIO $ GPU.setFullscreen (GPU.bool ef) (GPU.bool udr)

getActiveTarget :: forall m. MonadIO m => m (Ptr GPU.Target)
getActiveTarget = liftIO GPU.getActiveTarget

setActiveTarget :: forall m. MonadIO m => Ptr GPU.Target -> m Prelude.Bool
setActiveTarget t = fmap GPU.isTrue <$> liftIO $ GPU.setActiveTarget t

setShapeBlending :: forall m. MonadIO m => Prelude.Bool -> m ()
setShapeBlending b = liftIO $ GPU.setShapeBlending (GPU.bool b)

getBlendModeFromPreset
  :: forall m
   . MonadIO m
  => GPU.BlendPresetEnum
  -> m (GPU.BlendMode)
getBlendModeFromPreset preset = liftIO $ alloca $ \pbm -> GPU.getBlendModeFromPreset pbm preset >> peek pbm

setShapeBlendFunction
  :: forall m
   . MonadIO m
  => GPU.BlendFuncEnum -- ^ source_color
  -> GPU.BlendFuncEnum -- ^ dest_color
  -> GPU.BlendFuncEnum -- ^ source_alpha
  -> GPU.BlendFuncEnum -- ^ dest_alpha
  -> m ()
setShapeBlendFunction f1 f2 f3 f4 = liftIO $ GPU.setShapeBlendFunction f1 f2 f3 f4

setShapeBlendEquation
  :: forall m
   . MonadIO m
  => GPU.BlendEqEnum -- ^ color_equation
  -> GPU.BlendEqEnum -- ^ alpha_equation
  -> m ()
setShapeBlendEquation e1 e2 = liftIO $ GPU.setShapeBlendEquation e1 e2

setShapeBlendMode
  :: forall m
   . MonadIO m
  => GPU.BlendPresetEnum
  -> m ()
setShapeBlendMode = liftIO . GPU.setShapeBlendMode

setLineThickness
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ thickness
  -> m GPU.Float -- ^ the old thickness value
setLineThickness = liftIO . GPU.setLineThickness

getLineThickness
  :: forall m
   . MonadIO m
  => m GPU.Float
getLineThickness = liftIO GPU.getLineThickness
