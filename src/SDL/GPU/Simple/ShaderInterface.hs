module SDL.GPU.Simple.ShaderInterface where

import Prelude
import Data.Int
import Data.Foldable
import SDL.GPU.C as GPU
import qualified SDL.Raw.Types as SDL
import Control.Monad.IO.Class
import Foreign.C.String
import Foreign.Marshal.Array
import Foreign.Marshal.Utils (with)
import Data.Proxy
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Memorable
import GHC.TypeLits
import Linear.V

createShaderProgram :: forall m. MonadIO m => m GPU.ShaderProgram
createShaderProgram = liftIO GPU.createShaderProgram

freeShaderProgram :: forall m. MonadIO m => GPU.ShaderProgram -> m ()
freeShaderProgram = liftIO . GPU.freeShaderProgram

compileShaderRW
  :: forall m
   . MonadIO m
  => GPU.ShaderEnum
  -> Ptr SDL.RWops -- ^ shader_source
  -> GPU.Bool -- ^ free_rwops
  -> m GPU.Shader
-- NOTE: If we accidentally omit GPU. and create infinite recursion,
-- it for some reason isn't a GHC error despite this binding NOT being
-- IO, which violates liftIO's type. This is because GHC is treating
-- this function as IO instead of forall m. MonadIO m =>. No idea why.
--
-- To protect against it for now, I have imported GPU unqualified. So
-- failure to qualify will result in an ambiguous name.
compileShaderRW ty rwops b = liftIO $ GPU.compileShaderRW ty rwops b

compileShader
  :: forall m
   . MonadIO m
  => GPU.ShaderEnum
  -> String -- ^ shader_source
  -> m GPU.Shader
compileShader ty src = liftIO $ withCString src $ GPU.compileShader ty

loadShader
  :: forall m
   . MonadIO m
  => GPU.ShaderEnum
  -> FilePath -- ^ filename
  -> m GPU.Shader
loadShader ty fp = liftIO $ withCString fp $ \cfp -> GPU.loadShader ty cfp

linkShaders
  :: forall m
   . MonadIO m
  => GPU.Shader
  -> GPU.Shader
  -> m GPU.ShaderProgram
linkShaders s1 s2 = liftIO $ GPU.linkShaders s1 s2

linkManyShaders
  :: forall m
   . MonadIO m
  => [GPU.Shader]
  -> m GPU.ShaderProgram
linkManyShaders ss = liftIO $ withArray ss $ \css -> GPU.linkManyShaders css (fromIntegral $ length ss)

freeShader
  :: forall m
   . MonadIO m
  => GPU.Shader
  -> m ()
freeShader = liftIO . GPU.freeShader

attachShader
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> GPU.Shader
  -> m ()
attachShader sp s = liftIO $ GPU.attachShader sp s

detachShader
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> GPU.Shader
  -> m ()
detachShader sp s = liftIO $ GPU.detachShader sp s

linkShaderProgram
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> m Prelude.Bool
linkShaderProgram = fmap GPU.isTrue . liftIO . GPU.linkShaderProgram

getCurrentShaderProgram
  :: forall m
   . MonadIO m
  => m GPU.ShaderProgram
getCurrentShaderProgram = liftIO GPU.getCurrentShaderProgram

isDefaultShaderProgram
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> m Prelude.Bool
isDefaultShaderProgram = fmap GPU.isTrue . liftIO . GPU.isDefaultShaderProgram

-- | I've made this treat GPU.ShaderBlock as a value type.
-- It seems to be used that way, but the underlying function
-- _does_ take a pointer. But the simple-shader example passes
-- a pointer on the stack that is then returned by-value.
activateShaderProgram
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> GPU.ShaderBlock
  -> m ()
activateShaderProgram p block = liftIO $ with block $ GPU.activateShaderProgram p

deactivateShaderProgram
  :: forall m
   . MonadIO m
  => m ()
deactivateShaderProgram = liftIO GPU.deactivateShaderProgram

getShaderMessage :: forall m. MonadIO m => m String
getShaderMessage = liftIO $ GPU.getShaderMessage >>= peekCString

getAttributeLocation
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> String -- ^ attrib_name
  -> m GPU.Int
getAttributeLocation p s = liftIO $ withCString s \cs -> GPU.getAttributeLocation p cs

getUniformLocation
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> String -- ^ uniform_name
  -> m GPU.Int
getUniformLocation p s = liftIO $ withCString s \cs -> GPU.getUniformLocation p cs

loadShaderBlock
  :: forall m
   . MonadIO m
  => GPU.ShaderProgram
  -> String -- ^ position_name
  -> String -- ^ texcoord_name
  -> String -- ^ color_name
  -> String -- ^ modelViewMatrix_name
  -> m GPU.ShaderBlock
loadShaderBlock p s1 s2 s3 s4 = liftIO $ alloca \psb -> do
  withCString s1 \cs1 ->
    withCString s2 \cs2 ->
    withCString s3 \cs3 ->
    withCString s4 \cs4 ->
    GPU.loadShaderBlock psb p cs1 cs2 cs3 cs4
  peek psb

setShaderBlock :: forall m. MonadIO m => GPU.ShaderBlock -> m ()
setShaderBlock sb = liftIO $ with sb \psb -> liftIO $ GPU.setShaderBlock psb

getShaderBlock :: forall m. MonadIO m => m GPU.ShaderBlock
getShaderBlock = liftIO $ alloca $ \psb ->  GPU.getShaderBlock psb >> peek psb

setShaderImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Int -- ^ location
  -> GPU.Int -- ^ image_unit
  -> m ()
setShaderImage img loc u = liftIO $ GPU.setShaderImage img loc u


setUniformf
  :: forall m
   . MonadIO m
  => GPU.Int -- ^ location
  -> GPU.Float -- ^ value
  -> m ()
setUniformf loc f = liftIO $ GPU.setUniformf loc f

setUniformfv
  :: forall m v
   . MonadIO m
  => Finite v
  => KnownNat (Size v)
  => GPU.Int -- ^ TODO: newtype as GPU.ShaderLocation
  -> v GPU.Float
  -> m ()
setUniformfv loc v = liftIO $ do
  let v'len = fromIntegral $ natVal $ Proxy @(Size v)
  withArray (toList $ toV v) $ \varr -> GPU.setUniformfv loc v'len 1 varr

setUniformfvs
  :: forall m v
   . MonadIO m
  => Finite v
  => KnownNat (Size v)
  => GPU.Int -- ^ TODO: newtype as GPU.ShaderLocation
  -> [v GPU.Float]
  -> m ()
setUniformfvs loc vs = liftIO $ do
  let vs'len = fromIntegral $ length vs
      v'len = fromIntegral $ natVal $ Proxy @(Size v)
  withArray (vs >>= toList . toV) $ \vsarr -> GPU.setUniformfv loc v'len vs'len vsarr

-- TODO: Still more to bind, but this should be enough for the simple-shader demo
