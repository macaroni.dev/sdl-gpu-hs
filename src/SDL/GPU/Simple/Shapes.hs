module SDL.GPU.Simple.Shapes where


import Data.Word
import Data.Foldable (toList)
import Foreign.Ptr
import Foreign.Marshal.Utils
import Foreign.Marshal.Array
import Control.Monad.IO.Class
import Linear.V2

import SDL.GPU.C as GPU

pixel
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Color
  -> m ()
pixel t x y c = liftIO $ with c $ GPU.pixel t x y

line
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Color
  -> m ()
line t x1 y1 x2 y2 c = liftIO $ with c $ GPU.line t x1 y1 x2 y2

arc
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> GPU.Color
  -> m ()
arc t x y r sa ea c = liftIO $ with c $ GPU.arc t x y r sa ea

arcFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> GPU.Color
  -> m ()
arcFilled t x y r sa ea c = liftIO $ with c $ GPU.arcFilled t x y r sa ea

circle
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> GPU.Color
  -> m ()
circle t x y r c = liftIO $ with c $ GPU.circle t x y r

circleFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> GPU.Color
  -> m ()
circleFilled t x y r c = liftIO $ with c $ GPU.circleFilled t x y r

ellipse
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ rx
  -> GPU.Float -- ^ ry
  -> GPU.Float -- ^ degrees
  -> GPU.Color
  -> m ()
ellipse t x y rx ry d c = liftIO $ with c $ GPU.ellipse t x y rx ry d

ellipseFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ rx
  -> GPU.Float -- ^ ry
  -> GPU.Float -- ^ degrees
  -> GPU.Color
  -> m ()
ellipseFilled t x y rx ry d c = liftIO $ with c $ GPU.ellipseFilled t x y rx ry d

sector
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ inner_radius
  -> GPU.Float -- ^ outer_radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> GPU.Color
  -> m ()
sector t x y ir or sa ea c = liftIO $ with c $ GPU.sector t x y ir or sa ea

sectorFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ inner_radius
  -> GPU.Float -- ^ outer_radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> GPU.Color
  -> m ()
sectorFilled t x y ir or sa ea c = liftIO $ with c $ GPU.sectorFilled t x y ir or sa ea

tri
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ x3
  -> GPU.Float -- ^ y3
  -> GPU.Color
  -> m ()
tri t x1 y1 x2 y2 x3 y3 c = liftIO $ with c $ GPU.tri t x1 y1 x2 y2 x3 y3

triFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ x3
  -> GPU.Float -- ^ y3
  -> GPU.Color
  -> m ()
triFilled t x1 y1 x2 y2 x3 y3 c = liftIO $ with c $ GPU.triFilled t x1 y1 x2 y2 x3 y3

rectangle
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Color
  -> m ()
rectangle t x1 y1 x2 y2 c = liftIO $ with c $ GPU.rectangle t x1 y1 x2 y2

rectangle2
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Rect
  -> GPU.Color
  -> m ()
rectangle2 t r c = liftIO $ with c $ \pc -> with r $ \pr -> GPU.rectangle2 t pr pc

rectangleFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Color
  -> m ()
rectangleFilled t x1 y1 x2 y2 c = liftIO $ with c $ GPU.rectangleFilled t x1 y1 x2 y2

rectangleFilled2
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Rect
  -> GPU.Color
  -> m ()
rectangleFilled2 t r c = liftIO $ with c $ \pc -> with r $ \pr -> GPU.rectangleFilled2 t pr pc

rectangleRound
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ radius
  -> GPU.Color
  -> m ()
rectangleRound t x1 y1 x2 y2 r c = liftIO $ with c $ GPU.rectangleRound t x1 y1 x2 y2 r

rectangleRound2
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Rect
  -> GPU.Float -- ^ radius
  -> GPU.Color
  -> m ()
rectangleRound2 t r rad c = liftIO $ with c $ \pc -> with r $ \pr -> GPU.rectangleRound2 t pr rad pc

rectangleRoundFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ radius
  -> GPU.Color
  -> m ()
rectangleRoundFilled t x1 y1 x2 y2 r c = liftIO $ with c $ GPU.rectangleRoundFilled t x1 y1 x2 y2 r

rectangleRoundFilled2
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Rect
  -> GPU.Float -- ^ radius
  -> GPU.Color
  -> m ()
rectangleRoundFilled2 t r rad c = liftIO $ with c $ \pc -> with r $ \pr -> GPU.rectangleRoundFilled2 t pr rad pc

polygon
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> [V2 GPU.Float] -- ^ vertices
  -> GPU.Color
  -> m ()
polygon t vs c = liftIO $ with c $ \pc -> withArray (vs >>= toList) $ \var -> GPU.polygon t (fromIntegral $ length vs) var pc

polyline
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> [V2 GPU.Float] -- ^ vertices
  -> GPU.Color
  -> Prelude.Bool -- ^ close_loop
  -> m ()
polyline t vs c b = liftIO $ with c $ \pc -> withArray (vs >>= toList) $ \var -> GPU.polyline t (fromIntegral $ length vs) var pc (GPU.bool b)

polygonFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> [V2 GPU.Float] -- ^ vertices
  -> GPU.Color
  -> m ()
polygonFilled t vs c = liftIO $ with c $ \pc -> withArray (vs >>= toList) $ \var -> GPU.polygonFilled t (fromIntegral $ length vs) var pc
