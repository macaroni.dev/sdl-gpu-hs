module SDL.GPU.Simple.Conversions where

import Data.Word
import Foreign.Ptr
import Control.Monad.IO.Class

import qualified SDL.Raw.Types as SDL.Raw
import qualified SDL

import SDL.GPU.C as GPU

copyImageFromRawSurface
  :: forall m
   . MonadIO m
  => Ptr SDL.Raw.Surface
  -> m (Ptr GPU.Image)
copyImageFromRawSurface = liftIO . GPU.copyImageFromSurface

copyImageFromSurface
  :: forall m
   . MonadIO m
  => SDL.Surface
  -> m (Ptr GPU.Image)
copyImageFromSurface (SDL.Surface ptr _) = liftIO $ GPU.copyImageFromSurface ptr

copyImageFromTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m (Ptr GPU.Image)
copyImageFromTarget = liftIO . GPU.copyImageFromTarget

copyRawSurfaceFromTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m (Ptr SDL.Raw.Surface)
copyRawSurfaceFromTarget = liftIO . GPU.copySurfaceFromTarget

copySurfaceFromTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m (SDL.Surface)
copySurfaceFromTarget t =
  fmap (Prelude.flip SDL.Surface Nothing) $ liftIO $ GPU.copySurfaceFromTarget t

copyRawSurfaceFromImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m (Ptr SDL.Raw.Surface)
copyRawSurfaceFromImage = liftIO . GPU.copySurfaceFromImage

copySurfaceFromImage
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m (SDL.Surface)
copySurfaceFromImage t =
  fmap (Prelude.flip SDL.Surface Nothing) $ liftIO $ GPU.copySurfaceFromImage t

