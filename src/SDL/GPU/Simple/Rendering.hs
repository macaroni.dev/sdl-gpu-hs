module SDL.GPU.Simple.Rendering where


import Data.Word
import Data.Void
import Foreign.Ptr
import Foreign.Marshal.Utils
import Control.Monad.IO.Class

import SDL.GPU.C as GPU

clear
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
clear = liftIO . GPU.clear

clearColor
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Color
  -> m ()
clearColor t c = liftIO $ with c $ \pc -> GPU.clearColor t pc

blit
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Maybe GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> m ()
blit img rect t x y = liftIO $ maybeWith with rect $ \pr ->
  GPU.blit img pr t x y

blitRotate
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Maybe GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ degrees
  -> m ()
blitRotate img rect t x y d = liftIO $ maybeWith with rect $ \pr ->
  GPU.blitRotate img pr t x y d

blitScale
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Maybe GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ scaleX
  -> GPU.Float -- ^ scaleY
  -> m ()
blitScale img rect t x y sx sy = liftIO $ maybeWith with rect $ \pr ->
  GPU.blitScale img pr t x y sx sy

blitTransform
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Maybe GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ scaleX
  -> GPU.Float -- ^ scaleY
  -> m ()
blitTransform img rect t x y d sx sy = liftIO $ maybeWith with rect $ \pr ->
  GPU.blitTransform img pr t x y d sx sy

blitTransformX
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> Maybe GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ pivot_x
  -> GPU.Float -- ^ pivot_y
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ scaleX
  -> GPU.Float -- ^ scaleY
  -> m ()
blitTransformX img rect t x y px py d sx sy = liftIO $ maybeWith with rect $ \pr ->
  GPU.blitTransformX img pr t x y px py d sx sy

blitRect
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Rect -- ^ src_rect
  -> Ptr GPU.Target
  -> GPU.Rect -- ^ dest_rect
  -> m ()
blitRect img src_rect t dest_rect = liftIO $
  with src_rect $ \ps ->
  with dest_rect $ \pd -> 
  GPU.blitRect img ps t pd

blitRectX
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> GPU.Rect -- ^ src_rect
  -> Ptr GPU.Target
  -> GPU.Rect -- ^ dest_rect
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ pivot_x
  -> GPU.Float -- ^ pivot_y
  -> GPU.FlipEnum -- ^ flip_direction
  -> m ()
blitRectX img src_rect t dest_rect d px py fd = liftIO $
  with src_rect $ \ps ->
  with dest_rect $ \pd -> 
  GPU.blitRectX img ps t pd d px py fd
-- TODO triangle/primitive batch

flushBlitBuffer
  :: forall m
   . MonadIO m
  => m ()
flushBlitBuffer = liftIO GPU.flushBlitBuffer

flip
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
flip = liftIO . GPU.flip
