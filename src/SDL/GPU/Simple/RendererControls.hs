module SDL.GPU.Simple.RendererControls where

import Data.Int
import Data.Word
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Alloc
import Control.Monad.IO.Class
import Linear.V2

import SDL.GPU.C as GPU

reserveNextRendererEnum
  :: forall m
   . MonadIO m
  => m GPU.RendererEnum
reserveNextRendererEnum = liftIO GPU.reserveNextRendererEnum

getNumActiveRenderers
  :: forall m
   . MonadIO m
  => m GPU.Int
getNumActiveRenderers = liftIO GPU.getNumActiveRenderers

getActiveRendererList
  :: forall m
   . MonadIO m
  => Ptr GPU.RendererID -- ^ renderers_array
  -> m ()
getActiveRendererList = liftIO . GPU.getActiveRendererList

getCurrentRenderer
  :: forall m
   . MonadIO m
  => m (Ptr GPU.Renderer)
getCurrentRenderer = liftIO GPU.getCurrentRenderer

setCurrentRenderer
  :: forall m
   . MonadIO m
  => Ptr GPU.RendererID
  -> m ()
setCurrentRenderer = liftIO . GPU.setCurrentRenderer

getRenderer
  :: forall m
   . MonadIO m
  => Ptr GPU.RendererID
  -> m (Ptr GPU.Renderer)
getRenderer = liftIO . GPU.getRenderer

freeRenderer
  :: forall m
   . MonadIO m
  => Ptr GPU.Renderer
  -> m ()
freeRenderer = liftIO . GPU.freeRenderer

resetRendererState
  :: forall m
   . MonadIO m
  => m ()
resetRendererState = liftIO GPU.resetRendererState

setCoordinateMode
  :: forall m
   . MonadIO m
  => GPU.Bool -- ^ use_math_coords
  -> m ()
setCoordinateMode = liftIO . GPU.setCoordinateMode

getCoordinateMode
  :: forall m
   . MonadIO m
  => m (GPU.Bool) -- ^ use_math_coords
getCoordinateMode = liftIO GPU.getCoordinateMode

setDefaultAnchor
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ anchor_x
  -> GPU.Float -- ^ anchor_y
  -> m ()
setDefaultAnchor x y = liftIO $ GPU.setDefaultAnchor x y

getDefaultAnchor
  :: forall m
   . MonadIO m
  => m (V2 GPU.Float)
getDefaultAnchor = liftIO $ alloca $ \x -> alloca $ \y -> do
  GPU.getDefaultAnchor x x
  V2 <$> peek x <*> peek y
