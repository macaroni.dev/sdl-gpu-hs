module SDL.GPU.Simple.Matrix where

import Data.Word
import Foreign.Ptr
import Foreign.C.String
import Control.Monad.IO.Class

import SDL.GPU.C as GPU


---
-- the good stuff

resetProjection
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
resetProjection = liftIO . GPU.resetProjection

matrixMode
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.MatrixMode
  -> m ()
matrixMode t m = liftIO $ GPU.matrixMode t m

-- TODO: setMVP
-- TODO: setMVPFromStack

pushMatrix
  :: forall m
   . MonadIO m
  => m ()
pushMatrix = liftIO GPU.pushMatrix

popMatrix
  :: forall m
   . MonadIO m
  => m ()
popMatrix = liftIO GPU.popMatrix

loadIdentity
  :: forall m
   . MonadIO m
  => m ()
loadIdentity = liftIO GPU.loadIdentity
-- TODO: loadMatrix

ortho
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ left
  -> GPU.Float -- ^ right
  -> GPU.Float -- ^ bottom
  -> GPU.Float -- ^ top
  -> GPU.Float -- ^ near
  -> GPU.Float -- ^ far
  -> m ()
ortho n1 n2 n3 n4 n5 n6 = liftIO $ GPU.ortho n1 n2 n3 n4 n5 n6

frustum
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ left
  -> GPU.Float -- ^ right
  -> GPU.Float -- ^ bottom
  -> GPU.Float -- ^ top
  -> GPU.Float -- ^ near
  -> GPU.Float -- ^ far
  -> m ()
frustum n1 n2 n3 n4 n5 n6 = liftIO $ GPU.frustum n1 n2 n3 n4 n5 n6

perspective
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ fovy
  -> GPU.Float -- ^ aspect
  -> GPU.Float -- ^ z_near
  -> GPU.Float -- ^ z_far
  -> m ()
perspective n1 n2 n3 n4 = liftIO $ GPU.perspective n1 n2 n3 n4

lookAt
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ eye_x
  -> GPU.Float -- ^ eye_y
  -> GPU.Float -- ^ eye_z
  -> GPU.Float -- ^ target_x
  -> GPU.Float -- ^ target_y
  -> GPU.Float -- ^ target_z
  -> GPU.Float -- ^ up_x
  -> GPU.Float -- ^ up_y
  -> GPU.Float -- ^ up_z
  -> m ()
lookAt n1 n2 n3 n4 n5 n6 n7 n8 n9 =
  liftIO $ GPU.lookAt n1 n2 n3 n4 n5 n6 n7 n8 n9

translate
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ z
  -> m ()
translate x y z = liftIO $ GPU.translate x y z

scale
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ sx
  -> GPU.Float -- ^ sy
  -> GPU.Float -- ^ sz
  -> m ()
scale x y z = liftIO $ GPU.scale x y z

rotate
  :: forall m
   . MonadIO m
  => GPU.Float -- ^ degrees
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ z
  -> m ()
rotate d x y z = liftIO $ GPU.rotate d x y z

-- TODO: multMatrix
