module SDL.GPU.Simple.Initialization where

import Data.Int
import Data.Word
import Foreign.C.Types
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Marshal.Utils (with)
import Control.Monad.IO.Class
import Memorable

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C as GPU

setInitWindow
  :: forall m
   . MonadIO m
  => GPU.Uint32 -- ^ windowID
  -> m ()
setInitWindow = liftIO . GPU.setInitWindow

getInitWindow
  :: forall m
   . MonadIO m
  => m GPU.Uint32
getInitWindow = liftIO getInitWindow

getLinkedVersion
  :: forall m
   . MonadIO m
  => m SDL.Version
getLinkedVersion = liftIO $ initMem GPU.getLinkedVersion

setPreInitFlags
  :: forall m
   . MonadIO m
  => GPU.InitFlagEnum -- ^ GPU_flags
  -> m ()
setPreInitFlags = liftIO . GPU.setPreInitFlags

getPreInitFlags
  :: forall m
   . MonadIO m
  => m GPU.InitFlagEnum
getPreInitFlags = liftIO GPU.getPreInitFlags

setRequiredFeatures
  :: forall m
   . MonadIO m
  => GPU.FeatureEnum -- ^ GPU_flags
  -> m ()
setRequiredFeatures = liftIO . GPU.setRequiredFeatures

getRequiredFeatures
  :: forall m
   . MonadIO m
  => m GPU.FeatureEnum
getRequiredFeatures = liftIO GPU.getRequiredFeatures

getDefaultRendererOrder
  :: forall m
   . MonadIO m
  => m [GPU.RendererID]
getDefaultRendererOrder = liftIO $
  allocaArray @GPU.RendererID GPU.rendererOrderMax $ \arr ->
    alloca $ \psize -> do
      GPU.getDefaultRendererOrder psize arr
      size <- peek psize
      peekArray size arr

getRendererOrder
  :: forall m
   . MonadIO m
  => m [GPU.RendererID]
getRendererOrder = liftIO $
  allocaArray @GPU.RendererID GPU.rendererOrderMax $ \arr ->
  alloca $ \psize -> do
    GPU.getRendererOrder psize arr
    size <- peek psize
    peekArray (fromIntegral size) arr

setRendererOrder
  :: forall m
   . MonadIO m
  => [GPU.RendererID] -- ^ order
  -> m ()
setRendererOrder order = liftIO $ withArray order $ GPU.setRendererOrder (length order)

init
  :: forall m
   . MonadIO m
  => Word16 -- ^ w
  -> Word16 -- ^ h
  -> GPU.WindowFlagEnum
  -> m (Ptr GPU.Target)
init w h f = liftIO $ GPU.init w h f

initRenderer
  :: forall m
   . MonadIO m
  => GPU.RendererEnum
  -> Word16 -- ^ w
  -> Word16 -- ^ h
  -> GPU.WindowFlagEnum
  -> m (Ptr GPU.Target)
initRenderer r w h f = liftIO $ GPU.initRenderer r w h f

initRendererByID
  :: forall m
   . MonadIO m
  => Ptr GPU.RendererID
  -> Word16 -- ^ w
  -> Word16 -- ^ h
  -> GPU.WindowFlagEnum
  -> m (Ptr GPU.Target)
initRendererByID rid w h f = liftIO $ GPU.initRendererByID rid w h f

isFeatureEnabled
  :: forall m
   . MonadIO m
  => GPU.FeatureEnum
  -> m Prelude.Bool
isFeatureEnabled = fmap GPU.isTrue . liftIO . GPU.isFeatureEnabled

closeCurrentRenderer
  :: forall m
   . MonadIO m
  => m ()
closeCurrentRenderer = liftIO GPU.closeCurrentRenderer

quit
  :: forall m
   . MonadIO m
  => m ()
quit = liftIO GPU.quit
