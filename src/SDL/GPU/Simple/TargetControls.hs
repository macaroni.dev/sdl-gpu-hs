module SDL.GPU.Simple.TargetControls where

import Data.Int
import Data.Word
import Foreign.Ptr
import Foreign.Marshal.Utils
import Foreign.Marshal.Alloc
import Foreign.Storable
import Control.Monad.IO.Class
import Linear.V2

import SDL.GPU.C as GPU

createAliasTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m (Ptr GPU.Target)
createAliasTarget = liftIO . GPU.createAliasTarget

loadTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m (Ptr GPU.Target)
loadTarget = liftIO . GPU.loadTarget

getTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Image
  -> m (Ptr GPU.Target)
getTarget = liftIO . GPU.getTarget

freeTarget
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
freeTarget = liftIO . GPU.freeTarget

setVirtualResolution
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> m ()
setVirtualResolution t w h = liftIO $ GPU.setVirtualResolution t w h

getVirtualResolution
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m (V2 GPU.Uint16) -- ^ (w, h)
getVirtualResolution t = liftIO $ alloca $ \w -> alloca $ \h -> do
  GPU.getVirtualResolution t w h
  V2 <$> peek w <*> peek h

getVirtualCoords
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> V2 GPU.Float -- ^ display
  -> m (V2 GPU.Float)
getVirtualCoords t (V2 displayX displayY) = liftIO $ alloca $ \x -> alloca $ \y -> do
  GPU.getVirtualCoords t x y displayX displayY
  V2 <$> peek x <*> peek y

unsetVirtualResolution
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
unsetVirtualResolution = liftIO . GPU.unsetVirtualResolution

setViewport
 :: forall m
   . MonadIO m
 => Ptr GPU.Target
 -> GPU.Rect
 -> m ()
setViewport t rect = liftIO $ with rect $ GPU.setViewport t

unsetViewport
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
unsetViewport = liftIO . GPU.unsetViewport

getCamera
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m GPU.Camera
getCamera t = liftIO $ do
  alloca $ \cam -> do
    GPU.getCamera t cam
    peek cam

setCamera
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Camera -- ^ new camera
  -> m ()
setCamera t cam = liftIO $ with cam $ GPU.setCamera t

getDefaultCamera
  :: forall m
   . MonadIO m
  => m GPU.Camera -- ^ out
getDefaultCamera = liftIO $ alloca $ \cam -> do
  GPU.getDefaultCamera cam
  peek cam

-- TODO: setCameraReturning

enableCamera
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> Prelude.Bool -- ^ use_camera
  -> m ()
enableCamera t = liftIO . GPU.enableCamera t . GPU.bool

isCameraEnabled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m Prelude.Bool
isCameraEnabled = fmap GPU.isTrue . liftIO . GPU.isCameraEnabled

-- TODO: Depth stuff

-- TODO: getPixel

-- TODO: Clip stuff

-- TODO: insertsects

setTargetColor
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Color
  -> m ()
setTargetColor t c = liftIO $ with c $ GPU.setTargetColor t

setTargetRGB
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> m ()
setTargetRGB t r g b = liftIO $ GPU.setTargetRGB t r g b

setTargetRGBA
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> GPU.Uint8 -- ^ a
  -> m ()
setTargetRGBA t r g b a = liftIO $ GPU.setTargetRGBA t r g b a

unsetTargetColor
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> m ()
unsetTargetColor = liftIO . GPU.unsetTargetColor
