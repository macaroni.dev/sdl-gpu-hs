{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}

module SDL.GPU.FC.C.Types where

import Data.Bits
import Data.Word
import Foreign.Storable
import qualified SDL.Raw.Types as SDL
import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU
import Memorable
import GHC.Generics

#include <SDL_FontCache.h>

newtype AlignEnum = AlignEnum #{type FC_AlignEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

alignLeft :: AlignEnum
alignLeft = #{const FC_ALIGN_LEFT}

alignCenter :: AlignEnum
alignCenter = #{const FC_ALIGN_CENTER}

alignRight :: AlignEnum
alignRight = #{const FC_ALIGN_RIGHT}

newtype FilterEnum = FilterEnum #{type FC_FilterEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

filterNearest :: FilterEnum
filterNearest = #{const FC_FILTER_NEAREST}

filterLinear :: FilterEnum
filterLinear = #{const FC_FILTER_LINEAR}

data ScaleF mode = Scale
  { x :: MemField mode GPU.Float #{offset FC_Scale, x}
  , y :: MemField mode GPU.Float #{offset FC_Scale, y}
  }

deriving stock instance Generic (ScaleF mode)
type Scale = ScaleF HaskellMem
deriving stock instance Show Scale
deriving via (MemStorable Scale) instance Storable Scale
instance MemData Scale where
  memDataSize = #{size FC_Scale}
  memDataAlignment = #{alignment FC_Scale}

data EffectF mode = Effect
  { alignment :: MemField mode AlignEnum #{offset FC_Effect, alignment}
  , scale     :: MemField mode Scale #{offset FC_Effect, scale}
  , color     :: MemField mode SDL.Color #{offset FC_Effect, color}
  }

deriving stock instance Generic (EffectF mode)
type Effect = EffectF HaskellMem
deriving stock instance Show Effect
deriving via (MemStorable Effect) instance Storable Effect
instance MemData Effect where
  memDataSize = #{size FC_Effect}
  memDataAlignment = #{alignment FC_Effect}

-- Opaque type
data Font

data GlyphDataF mode = GlyphData
  { rect        :: MemField mode SDL.Rect #{offset FC_GlyphData, rect}
  , cache_level :: MemField mode GPU.Int #{offset FC_GlyphData, cache_level}
  }

deriving stock instance Generic (GlyphDataF mode)
type GlyphData = GlyphDataF HaskellMem
deriving stock instance Show GlyphData
deriving via (MemStorable GlyphData) instance Storable GlyphData
instance MemData GlyphData where
  memDataSize = #{size FC_GlyphData}
  memDataAlignment = #{alignment FC_GlyphData}
