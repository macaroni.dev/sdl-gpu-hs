module SDL.GPU.FC.C
  ( module SDL.GPU.FC.C
  ) where

import Data.Bits
import Data.Int
import Data.Void
import Data.Word
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String

import qualified SDL.Raw.Types as SDL
--import qualified SDL.Raw.Font as TTF
import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU
import SDL.GPU.FC.C.Types

import Memorable
import GHC.Generics

-- Object Creation
foreign import ccall safe "FC_Adapter_MakeRect" makeRect
  :: Ptr GPU.Rect -- ^ out
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ w
  -> GPU.Float -- ^ h
  -> IO ()

foreign import ccall safe "FC_Adapter_MakeScale" makeScale
  :: Ptr Scale -- ^ out
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> IO ()

foreign import ccall safe "FC_Adapter_MakeColor" makeColor
  :: Ptr SDL.Color
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> GPU.Uint8 -- ^ a
  -> IO ()

foreign import ccall safe "FC_Adapter_MakeEffect" makeEffect
  :: Ptr Effect -- ^ out
  -> AlignEnum
  -> Ptr Scale
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "FC_Adapter_MakeGlyphData" makeGlyphData
  :: Ptr GlyphData
  -> GPU.Int -- ^ cache_level
  -> GPU.Sint16 -- ^ x
  -> GPU.Sint16 -- ^ y
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> IO ()

-- Font object

foreign import ccall safe "SDL_FontCache.h FC_CreateFont" createFont
  :: IO (Ptr Font)

-- TODO: Do these return C true/false? Should we use GPU.Bool?

foreign import ccall safe "FC_Adapter_LoadFont" loadFont
  :: Ptr Font
  -> CString -- ^ filename_ttf
  -> GPU.Uint32 -- ^ pointSize
  -> Ptr SDL.Color
  -> GPU.Int -- ^ style
  -> IO GPU.Bool
{-
foreign import ccall safe "FC_Adapter_LoadFontFromTTF" loadFontFromTTF
  :: Ptr Font
  -> Ptr TTF.Font
  -> Ptr SDL.Color
  -> IO GPU.Bool
-}
foreign import ccall safe "FC_Adapter_LoadFont_RW" loadFontRW
  :: Ptr Font
  -> Ptr SDL.RWops
  -> GPU.Uint8 -- ^ own_rwops
  -> GPU.Uint32 -- ^ pointSize
  -> Ptr SDL.Color
  -> GPU.Int -- ^ style
  -> IO GPU.Bool

foreign import ccall safe "SDL_FontCache.h FC_ClearFont" clearFont
  :: Ptr Font
  -> IO ()

foreign import ccall safe "SDL_FontCache.h FC_FreeFont" freeFont
  :: Ptr Font
  -> IO ()

-- NOTE: Lots of FC_* functions unbound..but they do not seem to be part of the API

foreign import ccall safe "FC_Adapter_Draw" draw
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawAlign" drawAlign
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> AlignEnum
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawScale" drawScale
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> Ptr Scale
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawColor" drawColor
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> Ptr SDL.Color
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawEffect" drawEffect
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> Ptr Effect
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawBox" drawBox
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> Ptr GPU.Rect -- ^ box
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawBoxAlign" drawBoxAlign
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> Ptr GPU.Rect -- ^ box
  -> AlignEnum
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawBoxScale" drawBoxScale
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> Ptr GPU.Rect -- ^ box
  -> Ptr Scale
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawBoxColor" drawBoxColor
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> Ptr GPU.Rect -- ^ box
  -> Ptr SDL.Color
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawBoxEffect" drawBoxEffect
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> Ptr GPU.Rect -- ^ box
  -> Ptr Effect
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawColumn" drawColumn
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawColumnAlign" drawColumnAlign
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> AlignEnum
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawColumnScale" drawColumnScale
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> Ptr Scale
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawColumnColor" drawColumnColor
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> Ptr SDL.Color
  -> CString -- ^ text
  -> IO ()

foreign import ccall safe "FC_Adapter_DrawColumnEffect" drawColumnEffect
  :: Ptr GPU.Rect -- ^ out
  -> Ptr Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> Ptr Effect
  -> CString -- ^ text
  -> IO ()

-- INCOMPLETE: Getters & Settings

foreign import ccall safe "FC_SetFilterMode" setFilterMode
  :: Ptr Font
  -> FilterEnum
  -> IO ()
