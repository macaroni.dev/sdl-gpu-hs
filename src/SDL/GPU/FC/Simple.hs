module SDL.GPU.FC.Simple (module SDL.GPU.FC.Simple, module SDL.GPU.FC.C.Types) where

import Control.Monad (unless)
import Foreign.Ptr
import Foreign.Storable
import Foreign.C.String
import Foreign.Marshal.Utils
import Foreign.Marshal.Alloc
import Control.Monad.IO.Class

import Memorable
import qualified SDL.GPU.C as GPU
import SDL.GPU.FC.C as FC
import SDL.GPU.FC.C.Types as FC
import SDL.GPU.FC.C.Types

-- Object Creation

-- TODO: Do these return C true/false? Should we use GPU.Bool?

loadFont
  :: forall m
   . MonadIO m
  => String -- ^ filename_ttf
  -> GPU.Uint32 -- ^ pointSize
  -> GPU.Color
  -> GPU.Int -- ^ style - try 0
  -> m (Ptr FC.Font)
loadFont fp size c style = liftIO $ do
  font <- FC.createFont
  with c $ \pc -> withCString fp $ \cfp -> do
    success <- GPU.isTrue <$> FC.loadFont font cfp size pc style
    unless success $ do
      error $ unwords ["Failed to loadFont:", fp]
    pure font
{-
foreign import ccall safe "FC_Adapter_LoadFontFromTTF" loadFontFromTTF
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr TTF.Font
  -> Ptr GPU.Color
  -> m GPU.Bool
-}
-- TODO: RW

clearFont
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> m ()
clearFont = liftIO . FC.clearFont

freeFont
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> m ()
freeFont = liftIO . FC.freeFont
-- NOTE: Lots of FC_* functions unbound..but they do not seem to be part of the API

draw
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> String -- ^ text
  -> m GPU.Rect
draw font t x y txt = liftIO $ initMem $ \out -> withCString txt $ \ctxt -> do
  FC.draw out font t x y ctxt

drawAlign
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> FC.AlignEnum
  -> String -- ^ text
  -> m GPU.Rect
drawAlign font t x y align txt = liftIO $ initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawAlign out font t x y align ctxt


drawScale
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> FC.Scale
  -> String -- ^ text
  -> m GPU.Rect
drawScale font t x y scale txt = liftIO $ initMem $ \out -> withCString txt $ \ctxt -> with scale $ \pscale -> do
  FC.drawScale out font t x y pscale ctxt

drawColor
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Color
  -> String -- ^ text
  -> m GPU.Rect
drawColor font t x y c txt = liftIO $ with c $ \pc -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawColor out font t x y pc ctxt

drawEffect
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> FC.Effect
  -> String -- ^ text
  -> m GPU.Rect
drawEffect font t x y e txt = liftIO $ with e $ \eff -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawEffect out font t x y eff ctxt

drawBox
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Rect -- ^ box
  -> String -- ^ text
  -> m GPU.Rect
drawBox font t box txt = liftIO $ with box $ \pbox -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawBox out font t pbox ctxt



drawBoxAlign
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Rect -- ^ box
  -> FC.AlignEnum
  -> String -- ^ text
  -> m GPU.Rect
drawBoxAlign font t box align txt = liftIO $ with box $ \pbox -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawBoxAlign out font t pbox align ctxt




drawBoxScale
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Rect -- ^ box
  -> FC.Scale
  -> String -- ^ text
  -> m GPU.Rect
drawBoxScale font t box scale txt = liftIO $ with scale $ \pscale -> with box $ \pbox -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawBoxScale out font t pbox pscale ctxt


drawBoxColor
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Rect -- ^ box
  -> GPU.Color
  -> String -- ^ text
  -> m GPU.Rect
drawBoxColor font t box c txt = liftIO $ with c $ \pc -> with box $ \pbox -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawBoxColor out font t pbox pc ctxt


drawBoxEffect
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Rect -- ^ box
  -> FC.Effect
  -> String -- ^ text
  -> m GPU.Rect
drawBoxEffect font t box e txt = liftIO $ with e $ \pe -> with box $ \pbox -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawBoxEffect out font t pbox pe ctxt


drawColumn
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> String -- ^ text
  -> m GPU.Rect
drawColumn font t x y w txt = liftIO $ initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawColumn out font t x y w ctxt

drawColumnAlign
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> FC.AlignEnum
  -> String -- ^ text
  -> m GPU.Rect
drawColumnAlign font t x y w align txt = liftIO $ initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawColumnAlign out font t x y w align ctxt

drawColumnScale
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> FC.Scale
  -> String -- ^ text
  -> m GPU.Rect
drawColumnScale font t x y w s txt = liftIO $ with s $ \ps -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawColumnScale out font t x y w ps ctxt


drawColumnColor
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> GPU.Color
  -> String -- ^ text
  -> m GPU.Rect
drawColumnColor font t x y w c txt = liftIO $ with c $ \pc -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawColumnColor out font t x y w pc ctxt



drawColumnEffect
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> Ptr GPU.Target -- ^ dest
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Uint16 -- ^ width
  -> FC.Effect
  -> String -- ^ text
  -> m GPU.Rect
drawColumnEffect font t x y w e txt = liftIO $ with e $ \pe -> initMem $ \out -> withCString txt $ \ctxt -> do
  FC.drawColumnEffect out font t x y w pe ctxt

setFilterMode
  :: forall m
   . MonadIO m
  => Ptr FC.Font
  -> FC.FilterEnum
  -> m ()
setFilterMode font fe = liftIO $ FC.setFilterMode font fe
