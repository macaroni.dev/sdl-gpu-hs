module SDL.GPU.C.Matrix where

import Data.Word
import Foreign.Ptr
import Foreign.C.String

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

-- TODO: Can this be unsafe+pure?
foreign import ccall safe "SDL_gpu.h GPU_VectorLength" vectorLength
  :: Ptr GPU.Float -- ^ vec3
  -> IO GPU.Float

foreign import ccall safe "SDL_gpu.h GPU_VectorNormalize" vectorNormalize
  :: Ptr GPU.Float -- ^ vec3
  -> IO ()

-- TODO: Can this be unsafe+pure?
foreign import ccall safe "SDL_gpu.h GPU_VectorDot" vectorDot
  :: Ptr GPU.Float -- ^ A
  -> Ptr GPU.Float -- ^ B
  -> IO GPU.Float

foreign import ccall safe "SDL_gpu.h GPU_VectorCross" vectorCross
  :: Ptr GPU.Float -- ^ result
  -> Ptr GPU.Float -- ^ A
  -> Ptr GPU.Float -- ^ B
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_VectorCopy" vectorCopy
  :: Ptr GPU.Float -- ^ result
  -> Ptr GPU.Float -- ^ A
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_VectorApplyMatrix" vectorApplyMatrix
  :: Ptr GPU.Float -- ^ vec3
  -> Ptr GPU.Float -- ^ matrix_4x4
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixCopy" matrixCopy
  :: Ptr GPU.Float -- ^ result
  -> Ptr GPU.Float -- ^ A
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixIdentity" matrixIdentity
  :: Ptr GPU.Float -- ^ result
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixOrtho" matrixOrtho
  :: Ptr GPU.Float -- ^ result
  -> GPU.Float -- ^ left
  -> GPU.Float -- ^ right
  -> GPU.Float -- ^ bottom
  -> GPU.Float -- ^ top
  -> GPU.Float -- ^ near
  -> GPU.Float -- ^ far
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixFrustum" matrixFrustum
  :: Ptr GPU.Float -- ^ result
  -> GPU.Float -- ^ left
  -> GPU.Float -- ^ right
  -> GPU.Float -- ^ bottom
  -> GPU.Float -- ^ top
  -> GPU.Float -- ^ near
  -> GPU.Float -- ^ far
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixPerspective" matrixPerspective
  :: Ptr GPU.Float -- ^ result
  -> GPU.Float -- ^ fovy
  -> GPU.Float -- ^ aspect
  -> GPU.Float -- ^ z_near
  -> GPU.Float -- ^ z_far
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixLookAt" matrixLookAt
  :: Ptr GPU.Float -- ^ matrix
  -> GPU.Float -- ^ eye_x
  -> GPU.Float -- ^ eye_y
  -> GPU.Float -- ^ eye_z
  -> GPU.Float -- ^ target_x
  -> GPU.Float -- ^ target_y
  -> GPU.Float -- ^ target_z
  -> GPU.Float -- ^ up_x
  -> GPU.Float -- ^ up_y
  -> GPU.Float -- ^ up_z
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixTranslate" matrixTranslate
  :: Ptr GPU.Float -- ^ result
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ z
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixScale" matrixScale
  :: Ptr GPU.Float -- ^ result
  -> GPU.Float -- ^ sx
  -> GPU.Float -- ^ sy
  -> GPU.Float -- ^ sz
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixRotate" matrixRotate
  :: Ptr GPU.Float -- ^ result
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ z
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixMultiply" matrixMultiply
  :: Ptr GPU.Float -- ^ result
  -> Ptr GPU.Float -- ^ A
  -> Ptr GPU.Float -- ^ B
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MultiplyAndAssign" multiplyAndAssign
  :: Ptr GPU.Float -- ^ result
  -> Ptr GPU.Float -- ^ B
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetMatrixString" getMatrixString
  :: Ptr GPU.Float -- ^ A
  -> IO CString

foreign import ccall safe "SDL_gpu.h GPU_GetCurrentMatrix" getCurrentMatrix
  :: IO (Ptr GPU.Float)

foreign import ccall safe "SDL_gpu.h GPU_GetTopMatrix" getTopMatrix
  :: Ptr GPU.MatrixStack
  -> IO (Ptr GPU.Float)

foreign import ccall safe "SDL_gpu.h GPU_GetModel" getModel
  :: IO (Ptr GPU.Float)

foreign import ccall safe "SDL_gpu.h GPU_GetView" getView
  :: IO (Ptr GPU.Float)

foreign import ccall safe "SDL_gpu.h GPU_GetProjection" getProjection
  :: IO (Ptr GPU.Float)

foreign import ccall safe "SDL_gpu.h GPU_GetModelViewProjection" getModelViewProjection
  :: Ptr GPU.Float -- ^ result
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_CreateMatrixStack" createMatrixStack
  :: IO (Ptr GPU.MatrixStack)

foreign import ccall safe "SDL_gpu.h GPU_FreeMatrixStack" freeMatrixStack
  :: Ptr GPU.MatrixStack
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_InitMatrixStack" initMatrixStack
  :: Ptr GPU.MatrixStack
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_CopyMatrixStack" copyMatrixStack
  :: Ptr GPU.MatrixStack -- ^ source
  -> Ptr GPU.MatrixStack -- ^ dest
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_ClearMatrixStack" clearMatrixStack
  :: Ptr GPU.MatrixStack
  -> IO ()

---
-- the good stuff

foreign import ccall safe "SDL_gpu.h GPU_ResetProjection" resetProjection
  :: Ptr GPU.Target
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MatrixMode" matrixMode
  :: Ptr GPU.Target
  -> GPU.MatrixMode
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetProjection" setProjection
  :: Ptr GPU.Float
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetView" setView
  :: Ptr GPU.Float
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetModel" setModel
  :: Ptr GPU.Float
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetProjectionFromStack" setProjectionFromStack
  :: Ptr GPU.MatrixStack
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetViewFromStack" setViewFromStack
  :: Ptr GPU.MatrixStack
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetModelFromStack" setModelFromStack
  :: Ptr GPU.MatrixStack
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_PushMatrix" pushMatrix
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_PopMatrix" popMatrix
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_LoadIdentity" loadIdentity
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_LoadMatrix" loadMatrix
  :: Ptr GPU.Float -- ^ matrix4x4
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Ortho" ortho
  :: GPU.Float -- ^ left
  -> GPU.Float -- ^ right
  -> GPU.Float -- ^ bottom
  -> GPU.Float -- ^ top
  -> GPU.Float -- ^ near
  -> GPU.Float -- ^ far
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Frustum" frustum
  :: GPU.Float -- ^ left
  -> GPU.Float -- ^ right
  -> GPU.Float -- ^ bottom
  -> GPU.Float -- ^ top
  -> GPU.Float -- ^ near
  -> GPU.Float -- ^ far
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Perspective" perspective
  :: GPU.Float -- ^ fovy
  -> GPU.Float -- ^ aspect
  -> GPU.Float -- ^ z_near
  -> GPU.Float -- ^ z_far
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_LookAt" lookAt
  :: GPU.Float -- ^ eye_x
  -> GPU.Float -- ^ eye_y
  -> GPU.Float -- ^ eye_z
  -> GPU.Float -- ^ target_x
  -> GPU.Float -- ^ target_y
  -> GPU.Float -- ^ target_z
  -> GPU.Float -- ^ up_x
  -> GPU.Float -- ^ up_y
  -> GPU.Float -- ^ up_z
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Translate" translate
  :: GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ z
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Scale" scale
  :: GPU.Float -- ^ sx
  -> GPU.Float -- ^ sy
  -> GPU.Float -- ^ sz
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Rotate" rotate
  :: GPU.Float -- ^ degrees
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ z
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_MultMatrix" multMatrix
  :: Ptr GPU.Float -- ^ matrix4x4
  -> IO ()
