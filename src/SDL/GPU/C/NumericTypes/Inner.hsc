{-# LANGUAGE NoImplicitPrelude #-}

module SDL.GPU.C.NumericTypes.Inner where

import Data.Int
import Data.Word
import GHC.Float

#include <SDL_gpu.h>

type GPU_Int = #{type int}
type GPU_Float = #{type float}
type GPU_UnsignedInt = #{type unsigned int}
type GPU_UnsignedShort = #{type unsigned short}
type GPU_UnsignedChar = #{type unsigned char}
type GPU_Sint16 = #{type Sint16}
type GPU_Uint8 = #{type Uint8}
type GPU_Uint16 = #{type Uint16}
type GPU_Uint32 = #{type Uint32}
