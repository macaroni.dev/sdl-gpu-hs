module SDL.GPU.C.SurfaceControls where

import Data.Word
import Foreign.Ptr
import Foreign.C.String

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_LoadSurface" loadSurface
  :: CString -- ^ filename
  -> IO (Ptr SDL.Surface)

foreign import ccall safe "SDL_gpu.h GPU_LoadSurface_RW" loadSurfaceRW
  :: Ptr SDL.RWops
  -> GPU.Bool -- ^ free_rwops
  -> IO (Ptr SDL.Surface)

foreign import ccall safe "SDL_gpu.h GPU_SaveSurface" saveSurface
  :: Ptr SDL.Surface
  -> CString -- ^ filename
  -> GPU.FileFormatEnum
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_SaveSurface_RW" saveSurfaceRW
  :: Ptr SDL.Surface
  -> Ptr SDL.RWops
  -> GPU.Bool -- ^ free_rwops
  -> GPU.FileFormatEnum
  -> IO GPU.Bool
