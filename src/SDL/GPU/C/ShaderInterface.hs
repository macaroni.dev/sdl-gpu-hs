module SDL.GPU.C.ShaderInterface where

import Data.Int
import Data.Word
import Data.Void
import Foreign.Ptr
import Foreign.C.String

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

-- TODO: Did I newtype these Uint32s right? Are they all just shader program handles?
  
foreign import ccall safe "SDL_gpu.h GPU_CreateShaderProgram" createShaderProgram
  :: IO GPU.ShaderProgram

foreign import ccall safe "SDL_gpu.h GPU_FreeShaderProgram" freeShaderProgram
  :: GPU.ShaderProgram
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_CompileShader_RW" compileShaderRW
  :: GPU.ShaderEnum
  -> Ptr SDL.RWops -- ^ shader_source
  -> GPU.Bool -- ^ free_rwops
  -> IO GPU.Shader

foreign import ccall safe "SDL_gpu.h GPU_CompileShader" compileShader
  :: GPU.ShaderEnum
  -> CString -- ^ shader_source
  -> IO GPU.Shader

foreign import ccall safe "SDL_gpu.h GPU_LoadShader" loadShader
  :: GPU.ShaderEnum
  -> CString -- ^ filename
  -> IO GPU.Shader

foreign import ccall safe "SDL_gpu.h GPU_LinkShaders" linkShaders
  :: GPU.Shader -- ^ shader_object1
  -> GPU.Shader -- ^ shader_object2
  -> IO GPU.ShaderProgram

foreign import ccall safe "SDL_gpu.h GPU_LinkManyShaders" linkManyShaders
  :: Ptr GPU.Shader -- ^ shader_objects
  -> GPU.Int -- ^ count
  -> IO GPU.ShaderProgram

foreign import ccall safe "SDL_gpu.h GPU_FreeShader" freeShader
  :: GPU.Shader
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_AttachShader" attachShader
  :: GPU.ShaderProgram
  -> GPU.Shader
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_DetachShader" detachShader
  :: GPU.ShaderProgram
  -> GPU.Shader
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_LinkShaderProgram" linkShaderProgram
  :: GPU.ShaderProgram
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_GetCurrentShaderProgram" getCurrentShaderProgram
  :: IO GPU.ShaderProgram

foreign import ccall safe "SDL_gpu.h GPU_IsDefaultShaderProgram" isDefaultShaderProgram
  :: GPU.ShaderProgram
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_ActivateShaderProgram" activateShaderProgram
  :: GPU.ShaderProgram
  -> Ptr GPU.ShaderBlock
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_DeactivateShaderProgram" deactivateShaderProgram
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetShaderMessage" getShaderMessage
  :: IO CString

foreign import ccall safe "SDL_gpu.h GPU_GetAttributeLocation" getAttributeLocation
  :: GPU.ShaderProgram
  -> CString -- ^ attrib_name
  -> IO GPU.Int

foreign import ccall safe "SDL_gpu.h GPU_Adapter_MakeAttributeFormat" makeAttributeFormat
  :: Ptr GPU.AttributeFormat -- ^ out
  -> GPU.Int -- ^ num_elems_per_vertex
  -> GPU.TypeEnum -- ^ type
  -> GPU.Bool -- ^ normalize
  -> GPU.Int -- ^ stride_bytes
  -> GPU.Int -- ^ offset_bytes
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_MakeAttribute" makeAttribute
  :: Ptr GPU.Attribute -- ^ out
  -> GPU.Int -- ^ location
  -> Ptr Void -- ^ values
  -> Ptr GPU.AttributeFormat
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetUniformLocation" getUniformLocation
  :: GPU.ShaderProgram
  -> CString -- ^ uniform_name
  -> IO GPU.Int

foreign import ccall safe "SDL_gpu.h GPU_Adapter_LoadShaderBlock" loadShaderBlock
  :: Ptr GPU.ShaderBlock -- ^ out
  -> GPU.ShaderProgram
  -> CString -- ^ position_name
  -> CString -- ^ texcoord_name
  -> CString -- ^ color_name
  -> CString -- ^ modelViewMatrix_name
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetShaderBlock" setShaderBlock
  :: Ptr GPU.ShaderBlock
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetShaderBlock" getShaderBlock
  :: Ptr GPU.ShaderBlock -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetShaderImage" setShaderImage
  :: Ptr GPU.Image
  -> GPU.Int -- ^ location
  -> GPU.Int -- ^ image_unit
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetUniformiv" getUniformiv
  :: GPU.ShaderProgram
  -> GPU.Int -- ^ location
  -> Ptr GPU.Int -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformi" setUniformi
  :: GPU.Int -- ^ location
  -> Ptr GPU.Int -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformiv" setUniformiv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_elements_per_value
  -> GPU.Int -- ^ num_values
  -> Ptr GPU.Int -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetUniformuiv" getUniformuiv
  :: GPU.ShaderProgram
  -> GPU.Int -- ^ location
  -> Ptr GPU.UnsignedInt -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformui" setUniformui
  :: GPU.Int -- ^ location
  -> Ptr GPU.UnsignedInt -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformuiv" setUniformuiv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_elements_per_value
  -> GPU.Int -- ^ num_values
  -> Ptr GPU.UnsignedInt -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetUniformfv" getUniformfv
  :: GPU.ShaderProgram
  -> GPU.Int -- ^ location
  -> Ptr GPU.Float -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformf" setUniformf
  :: GPU.Int -- ^ location
  -> GPU.Float -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformfv" setUniformfv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_elements_per_value
  -> GPU.Int -- ^ num_values
  -> Ptr GPU.Float -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetUniformMatrixfv" getUniformMatrixfv
  :: GPU.ShaderProgram
  -> GPU.Int -- ^ location
  -> Ptr GPU.Float -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetUniformMatrixfv" setUniformMatrixfv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_matrices
  -> GPU.Int -- ^ num_rows
  -> GPU.Int -- ^ num_columns
  -> GPU.Bool    -- ^ transpose
  -> Ptr GPU.Float -- ^ values
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAttributef" setAttributef
  :: GPU.Int -- ^ location
  -> GPU.Float -- ^ value
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAttributei" setAttributei
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ value
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAttributeui" setAttributeui
  :: GPU.Int -- ^ location
  -> GPU.UnsignedInt -- ^ value
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAttributefv" setAttributefv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_elements
  -> Ptr GPU.Float -- ^ value
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAttributeiv" setAttributeiv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_elements
  -> Ptr GPU.Int -- ^ value
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAttributeuiv" setAttributeuiv
  :: GPU.Int -- ^ location
  -> GPU.Int -- ^ num_elements
  -> Ptr GPU.UnsignedInt -- ^ value
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetAttributeSource" setAttributeSource
  :: GPU.Int -- ^ num_values
  -> Ptr GPU.Attribute -- ^ source
  -> IO ()
