module SDL.GPU.C.ImageControls where

import Data.Word
import Data.Int
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_CreateImage" createImage
  :: GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> GPU.FormatEnum
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_CreateImageUsingTexture" createImageUsingTexture
  :: GPU.TextureHandle -- ^ handle
  -> GPU.Bool -- ^ take_ownership
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_LoadImage" loadImage
  :: CString
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_CreateAliasImage" createAliasImage
  :: Ptr GPU.Image
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_CopyImage" copyImage
  :: Ptr GPU.Image
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_FreeImage" freeImage
  :: Ptr GPU.Image
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetImageVirtualResolution" setImageVirtualResolution
  :: Ptr GPU.Image
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UnsetImageVirtualResolution" unsetImageVirtualResolution
  :: Ptr GPU.Image
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UpdateImage" updateImage
  :: Ptr GPU.Image
  -> Ptr GPU.Rect -- ^ image_rect
  -> Ptr SDL.Surface
  -> Ptr GPU.Rect -- ^ surface_rect
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UpdateImageBytes" updateImageBytes
  :: Ptr GPU.Image
  -> Ptr GPU.Rect -- ^ image_rect
  -> Ptr GPU.UnsignedChar -- ^ bytes
  -> GPU.Int -- ^ bytes_per_row
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_ReplaceImage" replaceImage
  :: Ptr GPU.Image
  -> Ptr SDL.Surface
  -> Ptr GPU.Rect -- ^ surface_rect
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_SaveImage" saveImage
  :: Ptr GPU.Image
  -> CString
  -> GPU.FileFormatEnum
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_SaveImage_RW" saveImageRW
  :: Ptr GPU.Image
  -> Ptr SDL.RWops
  -> GPU.Bool -- ^ free_rwops
  -> GPU.FileFormatEnum
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_GenerateMipmaps" generateMipmaps
  :: Ptr GPU.Image
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetColorPtr" setColor
  :: Ptr GPU.Image
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetRGB" setRGB
  :: Ptr GPU.Image
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetRGBA" setRGBA
  :: Ptr GPU.Image
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> GPU.Uint8 -- ^ a
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UnsetColor" unsetColor
  :: Ptr GPU.Image
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetBlending" getBlending
  :: Ptr GPU.Image
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_SetBlending" setBlending
  :: Ptr GPU.Image
  -> GPU.Bool
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetBlendFunction" setBlendFunction
  :: Ptr GPU.Image
  -> GPU.BlendFuncEnum -- ^ source_color
  -> GPU.BlendFuncEnum -- ^ dest_color
  -> GPU.BlendFuncEnum -- ^ source_alpha
  -> GPU.BlendFuncEnum -- ^ dest_alpha
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetBlendEquation" setBlendEquation
  :: Ptr GPU.Image
  -> GPU.BlendEqEnum -- ^ color_equation
  -> GPU.BlendEqEnum -- ^ alpha_equation
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetBlendMode" setBlendMode
  :: Ptr GPU.Image
  -> GPU.BlendPresetEnum -- ^ mode
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetImageFilter" setImageFilter
  :: Ptr GPU.Image
  -> GPU.FilterEnum -- ^ filter
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetAnchor" setAnchor
  :: Ptr GPU.Image
  -> GPU.Float -- ^ anchor_x
  -> GPU.Float -- ^ anchor_y
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetAnchor" getAnchor
  :: Ptr GPU.Image
  -> Ptr GPU.Float -- ^ anchor_x
  -> Ptr GPU.Float -- ^ anchor_y
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetSnapMode" getSnapMode
  :: Ptr GPU.Image
  -> IO GPU.SnapEnum

foreign import ccall safe "SDL_gpu.h GPU_SetSnapMode" setSnapMode
  :: Ptr GPU.Image
  -> GPU.SnapEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetWrapMode" setWrapMode
  :: Ptr GPU.Image
  -> GPU.WrapEnum -- ^ wrap_mode_x  
  -> GPU.WrapEnum -- ^ wrap_mode_y
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetTextureHandle" getTextureHandle
  :: Ptr GPU.Image
  -> IO GPU.TextureHandle
