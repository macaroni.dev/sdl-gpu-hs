module SDL.GPU.C.TargetControls where

import Data.Int
import Data.Word
import Foreign.Ptr

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_CreateAliasTarget" createAliasTarget
  :: Ptr GPU.Target
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_LoadTarget" loadTarget
  :: Ptr GPU.Image
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_GetTarget" getTarget
  :: Ptr GPU.Image
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_FreeTarget" freeTarget
  :: Ptr GPU.Target
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetVirtualResolution" setVirtualResolution
  :: Ptr GPU.Target
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetVirtualResolution" getVirtualResolution
  :: Ptr GPU.Target
  -> Ptr GPU.Uint16 -- ^ w
  -> Ptr GPU.Uint16 -- ^ h
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetVirtualCoords" getVirtualCoords
  :: Ptr GPU.Target
  -> Ptr GPU.Float -- ^ x
  -> Ptr GPU.Float -- ^ y
  -> GPU.Float -- ^ displayX
  -> GPU.Float -- ^ displayY
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UnsetVirtualResolution" unsetVirtualResolution
  :: Ptr GPU.Target
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_MakeRect" makeRect
  :: GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ w
  -> GPU.Float -- ^ h
  -> Ptr GPU.Rect
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_MakeColor" makeColor
  :: GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> GPU.Uint8 -- ^ a
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetViewport" setViewport
 :: Ptr GPU.Target
 -> Ptr GPU.Rect
 -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UnsetViewport" unsetViewport
  :: Ptr GPU.Target
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetCamera" getCamera
  :: Ptr GPU.Target
  -> Ptr GPU.Camera -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetCamera" setCamera
  :: Ptr GPU.Target
  -> Ptr GPU.Camera -- ^ new camera
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetDefaultCamera" getDefaultCamera
  :: Ptr GPU.Camera -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetCameraReturning" setCameraReturning
  :: Ptr GPU.Target
  -> Ptr GPU.Camera -- ^ new camera
  -> Ptr GPU.Camera -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_EnableCamera" enableCamera
  :: Ptr GPU.Target
  -> GPU.Bool -- ^ use_camera
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_IsCameraEnabled" isCameraEnabled
  :: Ptr GPU.Target
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_AddDepthBuffer" addDepthBuffer
  :: Ptr GPU.Target
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_SetDepthTest" setDepthTest
  :: Ptr GPU.Target
  -> GPU.Bool -- ^ enable
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetDepthWrite" setDepthWrite
  :: Ptr GPU.Target
  -> GPU.Bool -- ^ enable
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetDepthFunction" setDepthFunction
  :: Ptr GPU.Target
  -> GPU.ComparisonEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetPixel" getPixel
  :: Ptr GPU.Target
  -> GPU.Sint16 -- ^ x
  -> GPU.Sint16 -- ^ y
  -> Ptr SDL.Color -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetClipRect" setClipRect
  :: Ptr GPU.Target
  -> Ptr GPU.Rect -- ^ rect
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetClipRectReturning" setClipRectReturning
  :: Ptr GPU.Target
  -> Ptr GPU.Rect -- ^ rect
  -> Ptr GPU.Rect -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetClip" setClip
  :: Ptr GPU.Target
  -> GPU.Sint16 -- ^ x
  -> GPU.Sint16 -- ^ y
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetClipReturning" setClipReturning
  :: Ptr GPU.Target
  -> GPU.Sint16 -- ^ x
  -> GPU.Sint16 -- ^ y
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> Ptr GPU.Rect -- ^ out
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UnsetClip" unsetClip
  :: Ptr GPU.Target
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_IntersectRect" intersectRect
  :: Ptr GPU.Rect -- ^ A
  -> Ptr GPU.Rect -- ^ B
  -> Ptr GPU.Rect -- ^ result
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_Adapter_IntersectClipRect" intersectClipRect
  :: Ptr GPU.Target
  -> Ptr GPU.Rect -- ^ B
  -> Ptr GPU.Rect -- ^ result
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetTargetColor" setTargetColor
  :: Ptr GPU.Target
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetTargetRGB" setTargetRGB
  :: Ptr GPU.Target
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetTargetRGBA" setTargetRGBA
  :: Ptr GPU.Target
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> GPU.Uint8 -- ^ a
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_UnsetTargetColor" unsetTargetColor
  :: Ptr GPU.Target
  -> IO ()
