module SDL.GPU.C.ContextControls where

import Data.Word
import Foreign.Ptr

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_GetContextTarget" getContextTarget
  :: IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_GetWindowTarget" getWindowTarget
  :: GPU.Uint32 -- ^ windowID
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_CreateTargetFromWindow" createTargetFromWindow
  :: GPU.Uint32 -- ^ windowID
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_MakeCurrent" makeCurrent
  :: Ptr GPU.Target
  -> GPU.Uint32 -- ^ windowID
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetWindowResolution" setWindowResolution
  :: GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> IO (GPU.Bool)

foreign import ccall safe "SDL_gpu.h GPU_SetFullscreen" setFullscreen
  :: GPU.Bool -- ^ enable_fullscreen
  -> GPU.Bool -- ^ use_desktop_resolution
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_GetFullscreen" getFullscreen
  :: IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_GetActiveTarget" getActiveTarget
  :: IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_SetActiveTarget" setActiveTarget
  :: Ptr GPU.Target
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_SetShapeBlending" setShapeBlending
  :: GPU.Bool -- ^ enable
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetBlendModeFromPreset" getBlendModeFromPreset
  :: Ptr GPU.BlendMode -- ^ out
  -> GPU.BlendPresetEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetShapeBlendFunction" setShapeBlendFunction
  :: GPU.BlendFuncEnum -- ^ source_color
  -> GPU.BlendFuncEnum -- ^ dest_color
  -> GPU.BlendFuncEnum -- ^ source_alpha
  -> GPU.BlendFuncEnum -- ^ dest_alpha
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetShapeBlendEquation" setShapeBlendEquation
  :: GPU.BlendEqEnum -- ^ color_equation
  -> GPU.BlendEqEnum -- ^ alpha_equation
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetShapeBlendMode" setShapeBlendMode
  :: GPU.BlendPresetEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetLineThickness" setLineThickness
  :: GPU.Float -- ^ thickness
  -> IO GPU.Float -- ^ the old thickness value

foreign import ccall safe "SDL_gpu.h GPU_GetLineThickness" getLineThickness
  :: IO GPU.Float
