module SDL.GPU.C.Logging where

import Data.Word
import Foreign.Ptr
import Foreign.C.String

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_SetDebugLevel" setDebugLevel
  :: GPU.DebugLevelEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetDebugLevel" getDebugLevel
  :: IO GPU.DebugLevelEnum

-- NOTE - NOT BOUND: varargs GPU_Log* functions
-- NOTE - NOT BOUND: varargs GPU_SetLogCallback
-- NOTE - NOT BOUND: varargs GPU_PushErrorCode

foreign import ccall safe "SDL_gpu.h GPU_Adapter_PopErrorCode" popErrorCode
  :: Ptr GPU.ErrorObject
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetErrorString" getErrorString
  :: GPU.ErrorEnum
  -> IO CString

foreign import ccall safe "SDL_gpu.h GPU_SetErrorQueueMax" setErrorQueueMax
  :: GPU.UnsignedInt -- ^ max
  -> IO ()
