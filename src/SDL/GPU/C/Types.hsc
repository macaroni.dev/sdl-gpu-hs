{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}

module SDL.GPU.C.Types
  ( module SDL.GPU.C.Types
  , Color (..)) where

import Prelude hiding (Bool)
import qualified Prelude

import Data.Bits
import Data.Int
import Data.Word
import Data.Void
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String

import qualified SDL.Raw.Types as SDL
import SDL.Raw (Color (..))

import qualified SDL.GPU.C.NumericTypes as GPU
import qualified SDL.GPU.C.Bool as GPU
import Memorable
import GHC.Generics

#include <SDL_gpu.h>
-- #include <SDL_FontCache.h>

-- ttfStyleOutline :: Int
-- ttfStyleOutline = #{const TTF_STYLE_OUTLINE}

data ImageF mode = Image
  { renderer :: MemField mode (Ptr Renderer) #{offset GPU_Image, renderer}
  , context_target :: MemField mode (Ptr Target) #{offset GPU_Image, context_target}
  , target :: MemField mode (Ptr Target) #{offset GPU_Image, target}
  , w :: MemField mode GPU.Uint16 #{offset GPU_Image, w}
  , h :: MemField mode GPU.Uint16 #{offset GPU_Image, h}
  , using_virtual_resolution :: MemField mode GPU.Bool #{offset GPU_Image, using_virtual_resolution}
  , format :: MemField mode FormatEnum #{offset GPU_Image, format}
  , num_layers :: MemField mode GPU.Int #{offset GPU_Image, num_layers}
  , bytes_per_pixel :: MemField mode GPU.Int #{offset GPU_Image, bytes_per_pixel}
  , base_w :: MemField mode GPU.Uint16 #{offset GPU_Image, base_w}
  , base_h :: MemField mode GPU.Uint16 #{offset GPU_Image, base_h}
  , texture_w :: MemField mode GPU.Uint16 #{offset GPU_Image, texture_w}
  , texture_h :: MemField mode GPU.Uint16 #{offset GPU_Image, texture_h}
  , has_mipmaps :: MemField mode GPU.Bool #{offset GPU_Image, has_mipmaps}
  , anchor_x :: MemField mode GPU.Float #{offset GPU_Image, anchor_x}
  , anchor_y :: MemField mode GPU.Float #{offset GPU_Image, anchor_y}
  , color :: MemField mode SDL.Color #{offset GPU_Image, color}
  , use_blending :: MemField mode GPU.Bool #{offset GPU_Image, use_blending}
  , blend_mode :: MemField mode BlendMode #{offset GPU_Image, blend_mode}
  , filter_mode :: MemField mode FilterEnum #{offset GPU_Image, filter_mode}
  , snap_mode :: MemField mode SnapEnum #{offset GPU_Image, snap_mode}
  , wrap_mode_x :: MemField mode WrapEnum #{offset GPU_Image, wrap_mode_x}
  , wrap_mode_y :: MemField mode WrapEnum #{offset GPU_Image, wrap_mode_y}
  , data_ :: MemField mode (Ptr Void) #{offset GPU_Image, data}
  , refcount :: MemField mode GPU.Int #{offset GPU_Image, refcount}
  , is_alias :: MemField mode GPU.Bool #{offset GPU_Image, is_alias}
  } deriving stock (Generic)

type Image = ImageF HaskellMem
deriving stock instance Show Image
deriving via (MemStorable Image) instance Storable Image

instance MemData Image where
  memDataSize = #{size GPU_Image}
  memDataAlignment = #{alignment GPU_Image}

data RendererF mode = Renderer
  { id :: MemField mode RendererID #{offset GPU_Renderer, id}
  , requested_id :: MemField mode RendererID #{offset GPU_Renderer, requested_id}
  , _SDL_init_flags :: MemField mode WindowFlagEnum#{offset GPU_Renderer, SDL_init_flags}
  , _GPU_init_flags :: MemField mode InitFlagEnum#{offset GPU_Renderer, GPU_init_flags}
  , shader_language :: MemField mode ShaderLanguageEnum #{offset GPU_Renderer, shader_language}
  , min_shader_version :: MemField mode GPU.Int #{offset GPU_Renderer, min_shader_version}
  , max_shader_version :: MemField mode GPU.Int #{offset GPU_Renderer, max_shader_version}
  , enabled_features :: MemField mode FeatureEnum #{offset GPU_Renderer, enabled_features}
  , current_context_target :: MemField mode (Ptr Target) #{offset GPU_Renderer, current_context_target}
  , coordinate_mode :: MemField mode GPU.Bool #{offset GPU_Renderer, coordinate_mode}
  , default_image_anchor_x :: MemField mode GPU.Float #{offset GPU_Renderer, default_image_anchor_x}
  , default_image_anchor_y :: MemField mode GPU.Float #{offset GPU_Renderer, default_image_anchor_y}
  , impl :: MemField mode (Ptr RendererImpl) #{offset GPU_Renderer, impl}
  } deriving stock (Generic)

type Renderer = RendererF HaskellMem
deriving stock instance Show Renderer
deriving via (MemStorable Renderer) instance Storable Renderer
instance MemData Renderer where
  memDataSize = #{size GPU_Renderer}
  memDataAlignment = #{alignment GPU_Renderer}

-- | NOTE: GPU_RendererImpl is a C++ class under the hood
--
-- We will need a C wrapper to do anything useful with it
data RendererImpl

data Context -- TODO: bind struct

data RendererIDF mode = RendererID
  { name :: MemField mode CString #{offset GPU_RendererID, name}
  , renderer :: MemField mode RendererEnum #{offset GPU_RendererID, renderer}
  , major_version :: MemField mode GPU.Int #{offset GPU_RendererID, major_version}
  , minor_version :: MemField mode GPU.Int #{offset GPU_RendererID, minor_version}
  } deriving stock Generic

type RendererID = RendererIDF HaskellMem
deriving stock instance Show RendererID
deriving via (MemStorable RendererID) instance Storable RendererID
instance MemData RendererID where
  memDataSize = #{size GPU_RendererID}
  memDataAlignment = #{alignment GPU_RendererID}

newtype RendererEnum = RendererEnum #{type GPU_RendererEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

-- | invalid value
rendererUnknown :: RendererEnum
rendererUnknown = #{const GPU_RENDERER_UNKNOWN}

rendererOpenGL1Base :: RendererEnum
rendererOpenGL1Base = #{const GPU_RENDERER_OPENGL_1_BASE}

rendererOpenGL1 :: RendererEnum
rendererOpenGL1 = #{const GPU_RENDERER_OPENGL_1}

rendererOpenGL2 :: RendererEnum
rendererOpenGL2 = #{const GPU_RENDERER_OPENGL_2}

rendererOpenGL3 :: RendererEnum
rendererOpenGL3 = #{const GPU_RENDERER_OPENGL_3}

rendererOpenGL4 :: RendererEnum
rendererOpenGL4 = #{const GPU_RENDERER_OPENGL_4}

rendererGLES1 :: RendererEnum
rendererGLES1 = #{const GPU_RENDERER_GLES_1}

rendererGLES2 :: RendererEnum
rendererGLES2 = #{const GPU_RENDERER_GLES_2}

rendererGLES3 :: RendererEnum
rendererGLES3 = #{const GPU_RENDERER_GLES_3}

rendererD3D9 :: RendererEnum
rendererD3D9 = #{const GPU_RENDERER_D3D9}

rendererD3D10 :: RendererEnum
rendererD3D10 = #{const GPU_RENDERER_D3D10}

rendererD3D11 :: RendererEnum
rendererD3D11 = #{const GPU_RENDERER_D3D11}

rendererCustom0 :: RendererEnum
rendererCustom0 = #{const GPU_RENDERER_CUSTOM_0}

data TargetF mode = Target
  { renderer :: MemField mode (Ptr Renderer) #{offset GPU_Target, renderer}
  , context_target :: MemField mode (Ptr Target) #{offset GPU_Target, context_target}
  , image :: MemField mode (Ptr Image) #{offset GPU_Target, image}
  , data_ :: MemField mode (Ptr Void) #{offset GPU_Target, data}
  , w :: MemField mode GPU.Uint16 #{offset GPU_Target, w}
  , h :: MemField mode GPU.Uint16 #{offset GPU_Target, h}
  , using_virtual_resolution :: MemField mode GPU.Bool #{offset GPU_Target, using_virtual_resolution}
  , base_w :: MemField mode GPU.Uint16 #{offset GPU_Target, base_w}
  , base_h :: MemField mode GPU.Uint16 #{offset GPU_Target, base_h}
  , use_clip_rect :: MemField mode GPU.Bool #{offset GPU_Target, use_clip_rect}
  , clip_rect :: MemField mode Rect #{offset GPU_Target, clip_rect}
  , use_color :: MemField mode GPU.Bool #{offset GPU_Target, use_color}
  , color :: MemField mode SDL.Color #{offset GPU_Target, color}
  , viewport :: MemField mode Rect #{offset GPU_Target, viewport}
  , camera :: MemField mode Camera #{offset GPU_Target, camera}
  , use_camera :: MemField mode GPU.Bool #{offset GPU_Target, use_camera}
  , context :: MemField mode (Ptr Context) #{offset GPU_Target, context}
  , refcount :: MemField mode GPU.Int #{offset GPU_Target, refcount}
  , is_alias :: MemField mode GPU.Bool #{offset GPU_Target, is_alias}
  } deriving stock Generic

type Target = TargetF HaskellMem
deriving stock instance Show Target
deriving via (MemStorable Target) instance Storable Target
instance MemData Target where
  memDataSize = #{size GPU_Target}
  memDataAlignment = #{alignment GPU_Target}


data CameraF mode = Camera
  { x :: MemField mode GPU.Float #{offset GPU_Camera, x}
  , y :: MemField mode GPU.Float #{offset GPU_Camera, y}
  , z :: MemField mode GPU.Float #{offset GPU_Camera, z}
  , angle :: MemField mode GPU.Float #{offset GPU_Camera, angle}
  , zoom_x :: MemField mode GPU.Float #{offset GPU_Camera, zoom_x}
  , zoom_y :: MemField mode GPU.Float #{offset GPU_Camera, zoom_y}
  , z_near :: MemField mode GPU.Float #{offset GPU_Camera, z_near}
  , z_far :: MemField mode GPU.Float #{offset GPU_Camera, z_far}
  , use_centered_origin :: MemField mode GPU.Bool #{offset GPU_Camera, use_centered_origin}
  } 

deriving stock instance Generic (CameraF mode)
type Camera = CameraF HaskellMem
deriving stock instance Show Camera
deriving via (MemStorable Camera) instance Storable Camera
instance MemData Camera where
  memDataSize = #{size GPU_Camera}
  memDataAlignment = #{alignment GPU_Camera}

data RectF mode = Rect
  { x :: MemField mode GPU.Float #{offset GPU_Rect, x}
  , y :: MemField mode GPU.Float #{offset GPU_Rect, y}
  , w :: MemField mode GPU.Float #{offset GPU_Rect, w}
  , h :: MemField mode GPU.Float #{offset GPU_Rect, h}
  }

deriving stock instance Generic (RectF mode)
type Rect = RectF HaskellMem
deriving stock instance Show Rect
deriving via (MemStorable Rect) instance Storable Rect
instance MemData Rect where
  memDataSize = #{size GPU_Rect}
  memDataAlignment = #{alignment GPU_Rect}

newtype DebugLevelEnum = DebugLevelEnum #{type GPU_DebugLevelEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

debugLevel0 :: DebugLevelEnum
debugLevel0 = #{const GPU_DEBUG_LEVEL_0}

debugLevel1 :: DebugLevelEnum
debugLevel1 = #{const GPU_DEBUG_LEVEL_1}

debugLevel2 :: DebugLevelEnum
debugLevel2 = #{const GPU_DEBUG_LEVEL_2}

debugLevel3 :: DebugLevelEnum
debugLevel3 = #{const GPU_DEBUG_LEVEL_3}

debugLevelMax :: DebugLevelEnum
debugLevelMax = #{const GPU_DEBUG_LEVEL_MAX}

newtype ErrorEnum = ErrorEnum #{type GPU_ErrorEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

errorNone :: ErrorEnum
errorNone = #{const GPU_ERROR_NONE}

errorBackendError :: ErrorEnum
errorBackendError = #{const GPU_ERROR_BACKEND_ERROR}

errorDataError :: ErrorEnum
errorDataError = #{const GPU_ERROR_DATA_ERROR}

errorUserError :: ErrorEnum
errorUserError = #{const GPU_ERROR_USER_ERROR}

errorUnsupportedFunction :: ErrorEnum
errorUnsupportedFunction = #{const GPU_ERROR_UNSUPPORTED_FUNCTION}

errorNullArgument :: ErrorEnum
errorNullArgument = #{const GPU_ERROR_NULL_ARGUMENT}

errorFileNotFound :: ErrorEnum
errorFileNotFound = #{const GPU_ERROR_FILE_NOT_FOUND}

data ErrorObjectF mode = ErrorObject
  { function :: MemField mode CString #{offset GPU_ErrorObject, function}
  , error_   :: MemField mode ErrorEnum #{offset GPU_ErrorObject, error}
  , details  :: MemField mode CString #{offset GPU_ErrorObject, details}
  }

deriving stock instance Generic (ErrorObjectF mode)
type ErrorObject = ErrorObjectF HaskellMem
deriving stock instance Show ErrorObject
deriving via (MemStorable ErrorObject) instance Storable ErrorObject
instance MemData ErrorObject where
  memDataSize = #{size GPU_ErrorObject}
  memDataAlignment = #{alignment GPU_ErrorObject}

newtype WindowFlagEnum = WindowFlagEnum #{type GPU_WindowFlagEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

defaultInitFlags :: WindowFlagEnum
defaultInitFlags = #{const GPU_DEFAULT_INIT_FLAGS}

newtype InitFlagEnum = InitFlagEnum #{type GPU_InitFlagEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

initEnableVsync :: InitFlagEnum
initEnableVsync = #{const GPU_INIT_ENABLE_VSYNC}

-- TODO: Bind all init flags

data BlendModeF mode = BlendMode
  { source_color :: MemField mode BlendFuncEnum #{offset GPU_BlendMode, source_color}
  , dest_color :: MemField mode BlendFuncEnum #{offset GPU_BlendMode, dest_color}
  , source_alpha :: MemField mode BlendFuncEnum #{offset GPU_BlendMode, source_alpha}
  , dest_alpha :: MemField mode BlendFuncEnum #{offset GPU_BlendMode, dest_alpha}
  , color_equation :: MemField mode BlendEqEnum #{offset GPU_BlendMode, color_equation}
  , alpha_equation :: MemField mode BlendEqEnum #{offset GPU_BlendMode, alpha_equation}
  }

deriving stock instance Generic (BlendModeF mode)
type BlendMode = BlendModeF HaskellMem
deriving stock instance Show BlendMode
deriving via (MemStorable BlendMode) instance Storable BlendMode
instance MemData BlendMode where
  memDataSize = #{size GPU_BlendMode}
  memDataAlignment = #{alignment GPU_BlendMode}

newtype FeatureEnum = FeatureEnum #{type GPU_FeatureEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

newtype BlendFuncEnum = BlendFuncEnum #{type GPU_BlendFuncEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

funcZero :: BlendFuncEnum
funcZero = #{const GPU_FUNC_ZERO}

funcOne :: BlendFuncEnum
funcOne = #{const GPU_FUNC_ONE}

funcSrcColor :: BlendFuncEnum
funcSrcColor = #{const GPU_FUNC_SRC_COLOR}

funcDstColor :: BlendFuncEnum
funcDstColor = #{const GPU_FUNC_DST_COLOR}

funcOneMinusSrc :: BlendFuncEnum
funcOneMinusSrc = #{const GPU_FUNC_ONE_MINUS_SRC}

funcOneMinusDst :: BlendFuncEnum
funcOneMinusDst = #{const GPU_FUNC_ONE_MINUS_DST}

funcSrcAlpha :: BlendFuncEnum
funcSrcAlpha = #{const GPU_FUNC_SRC_ALPHA}

funcDstAlpha :: BlendFuncEnum
funcDstAlpha = #{const GPU_FUNC_DST_ALPHA}

funcOneMinusSrcAlpha :: BlendFuncEnum
funcOneMinusSrcAlpha = #{const GPU_FUNC_ONE_MINUS_SRC_ALPHA}

funcOneMinusDstAlpha :: BlendFuncEnum
funcOneMinusDstAlpha = #{const GPU_FUNC_ONE_MINUS_DST_ALPHA}

newtype BlendEqEnum = BlendEqEnum #{type GPU_BlendEqEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

eqAdd :: BlendEqEnum
eqAdd = #{const GPU_EQ_ADD}

eqSubtract :: BlendEqEnum
eqSubtract = #{const GPU_EQ_SUBTRACT}

eqReverseSubtract :: BlendEqEnum
eqReverseSubtract = #{const GPU_EQ_REVERSE_SUBTRACT}

newtype BlendPresetEnum = BlendPresetEnum #{type GPU_BlendPresetEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

blendNormal :: BlendPresetEnum
blendNormal = #{const GPU_BLEND_NORMAL}

blendPremultipliedAlpha :: BlendPresetEnum
blendPremultipliedAlpha = #{const GPU_BLEND_PREMULTIPLIED_ALPHA}

blendMultiply :: BlendPresetEnum
blendMultiply = #{const GPU_BLEND_MULTIPLY}

blendAdd :: BlendPresetEnum
blendAdd = #{const GPU_BLEND_ADD}

blendSubtract :: BlendPresetEnum
blendSubtract = #{const GPU_BLEND_SUBTRACT}

blendModAlpha :: BlendPresetEnum
blendModAlpha = #{const GPU_BLEND_MOD_ALPHA}

blendSetAlpha :: BlendPresetEnum
blendSetAlpha = #{const GPU_BLEND_SET_ALPHA}

blendSet :: BlendPresetEnum
blendSet = #{const GPU_BLEND_SET}

blendNormalKeepAlpha :: BlendPresetEnum
blendNormalKeepAlpha = #{const GPU_BLEND_NORMAL_KEEP_ALPHA}

blendNormalAddAlpha :: BlendPresetEnum
blendNormalAddAlpha = #{const GPU_BLEND_NORMAL_ADD_ALPHA}

blendNormalFactorAlpha :: BlendPresetEnum
blendNormalFactorAlpha = #{const GPU_BLEND_NORMAL_FACTOR_ALPHA}

newtype FilterEnum = FilterEnum #{type GPU_FilterEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

filterNearest :: FilterEnum
filterNearest = #{const GPU_FILTER_NEAREST}

filterLinear :: FilterEnum
filterLinear = #{const GPU_FILTER_LINEAR}

filterLinearMipmap :: FilterEnum
filterLinearMipmap = #{const GPU_FILTER_LINEAR_MIPMAP}

newtype SnapEnum = SnapEnum #{type GPU_SnapEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

snapNone :: SnapEnum
snapNone = #{const GPU_SNAP_NONE}

snapPosition :: SnapEnum
snapPosition = #{const GPU_SNAP_POSITION}

snapDimensions :: SnapEnum
snapDimensions = #{const GPU_SNAP_DIMENSIONS}

snapPositionAndDimensions :: SnapEnum
snapPositionAndDimensions = #{const GPU_SNAP_POSITION_AND_DIMENSIONS}

newtype WrapEnum = WrapEnum #{type GPU_WrapEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

wrapNone :: WrapEnum
wrapNone = #{const GPU_WRAP_NONE}

wrapRepeat :: WrapEnum
wrapRepeat = #{const GPU_WRAP_REPEAT}

wrapMirrored :: WrapEnum
wrapMirrored = #{const GPU_WRAP_MIRRORED}

newtype FormatEnum = FormatEnum #{type GPU_FormatEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

formatLuminance :: FormatEnum
formatLuminance = #{const GPU_FORMAT_LUMINANCE}

formatLuminanceAlpha :: FormatEnum
formatLuminanceAlpha = #{const GPU_FORMAT_LUMINANCE_ALPHA}

formatRGB :: FormatEnum
formatRGB = #{const GPU_FORMAT_RGB}

formatRGBA :: FormatEnum
formatRGBA = #{const GPU_FORMAT_RGBA}

formatAlpha :: FormatEnum
formatAlpha = #{const GPU_FORMAT_ALPHA}

formatRG :: FormatEnum
formatRG = #{const GPU_FORMAT_RG}

formatYCbCr422 :: FormatEnum
formatYCbCr422 = #{const GPU_FORMAT_YCbCr422}

formatYCbCr420P :: FormatEnum
formatYCbCr420P = #{const GPU_FORMAT_YCbCr420P}

newtype FileFormatEnum = FileFormatEnum #{type GPU_FileFormatEnum}
  deriving stock   (Eq, Show)
  deriving newtype (Num, Bits, Storable)

fileAuto :: FileFormatEnum
fileAuto = #{const GPU_FILE_AUTO}

filePNG :: FileFormatEnum
filePNG = #{const GPU_FILE_PNG}

fileBMP :: FileFormatEnum
fileBMP = #{const GPU_FILE_BMP}

fileTGA :: FileFormatEnum
fileTGA = #{const GPU_FILE_TGA}

data MatrixStack

-- | https://jsantell.com/model-view-projection/
newtype MatrixMode = MatrixMode GPU.Int
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

projection :: MatrixMode
projection = #{const GPU_PROJECTION}

view :: MatrixMode
view = #{const GPU_VIEW}

model :: MatrixMode
model = #{const GPU_MODEL}

newtype BatchFlagEnum = BatchFlagEnum #{type GPU_BatchFlagEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

batchXY :: BatchFlagEnum
batchXY = #{const GPU_BATCH_XY}

batchXYZ :: BatchFlagEnum
batchXYZ = #{const GPU_BATCH_XYZ}

batchST :: BatchFlagEnum
batchST = #{const GPU_BATCH_ST}

batchRGB :: BatchFlagEnum
batchRGB  = #{const GPU_BATCH_RGB}

batchRGBA :: BatchFlagEnum
batchRGBA = #{const GPU_BATCH_RGBA}

batchRGB8 :: BatchFlagEnum
batchRGB8 = #{const GPU_BATCH_RGB8}

batchRGBA8 :: BatchFlagEnum
batchRGBA8 = #{const GPU_BATCH_RGBA8}

newtype PrimitiveEnum = PrimitiveEnum #{type GPU_PrimitiveEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

points :: PrimitiveEnum
points = #{const GPU_POINTS}

lines :: PrimitiveEnum
lines = #{const GPU_LINES}

lineLoop :: PrimitiveEnum
lineLoop = #{const GPU_LINE_LOOP}

lineStrip :: PrimitiveEnum
lineStrip = #{const GPU_LINE_STRIP}

triangles:: PrimitiveEnum
triangles = #{const GPU_TRIANGLES}

triangleStrip :: PrimitiveEnum
triangleStrip = #{const GPU_TRIANGLE_STRIP}

triangleFan :: PrimitiveEnum
triangleFan = #{const GPU_TRIANGLE_FAN}

newtype FlipEnum = FlipEnum #{type GPU_FlipEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

flipNone :: FlipEnum
flipNone = #{const GPU_FLIP_NONE}

flipHorizontal :: FlipEnum
flipHorizontal = #{const GPU_FLIP_HORIZONTAL}

flipVertical :: FlipEnum
flipVertical = #{const GPU_FLIP_VERTICAL}

newtype TextureHandle = TextureHandle #{type GPU_TextureHandle}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

newtype ComparisonEnum = ComparisonEnum #{type GPU_ComparisonEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

never :: ComparisonEnum
never = #{const GPU_NEVER}

less :: ComparisonEnum
less = #{const GPU_LESS}

equal :: ComparisonEnum
equal = #{const GPU_EQUAL}

lequal:: ComparisonEnum
lequal = #{const GPU_LEQUAL}

greater :: ComparisonEnum
greater = #{const GPU_GREATER}

gequal :: ComparisonEnum
gequal = #{const GPU_GEQUAL}

always :: ComparisonEnum
always = #{const GPU_ALWAYS}

-- shader interface

data ShaderBlockF mode = ShaderBlock
  { position_loc :: MemField mode GPU.Int #{offset GPU_ShaderBlock, position_loc}
  , texcoord_loc :: MemField mode GPU.Int #{offset GPU_ShaderBlock, texcoord_loc}
  , color_loc :: MemField mode GPU.Int    #{offset GPU_ShaderBlock, color_loc}
  , modelViewProjection_loc :: MemField mode GPU.Int    #{offset GPU_ShaderBlock, modelViewProjection_loc}
  }

deriving stock instance Generic (ShaderBlockF mode)
type ShaderBlock = ShaderBlockF HaskellMem
deriving stock instance Show ShaderBlock
deriving via (MemStorable ShaderBlock) instance Storable ShaderBlock
instance MemData ShaderBlock where
  memDataSize = #{size GPU_ShaderBlock}
  memDataAlignment = #{alignment GPU_ShaderBlock}

-- | Note that the "default" value (returned by GPU_MakeAttributeFormat) for 'is_per_sprite' is 'False'
data AttributeFormatF mode = AttributeFormat
  { is_per_sprite :: MemField mode GPU.Bool #{offset GPU_AttributeFormat, is_per_sprite}
  , num_elems_per_value :: MemField mode GPU.Int #{offset GPU_AttributeFormat, num_elems_per_value}
  , type_ :: MemField mode TypeEnum #{offset GPU_AttributeFormat, type}
  , normalize :: MemField mode GPU.Bool #{offset GPU_AttributeFormat, normalize}
  , stride_bytes :: MemField mode GPU.Int #{offset GPU_AttributeFormat, stride_bytes}
  , offset_bytes :: MemField mode GPU.Int #{offset GPU_AttributeFormat, offset_bytes}
  }

deriving stock instance Generic (AttributeFormatF mode)
type AttributeFormat = AttributeFormatF HaskellMem
deriving stock instance Show AttributeFormat
deriving via (MemStorable AttributeFormat) instance Storable AttributeFormat
instance MemData AttributeFormat where
  memDataSize = #{size GPU_AttributeFormat}
  memDataAlignment = #{alignment GPU_AttributeFormat}

data AttributeF mode = Attribute
  { location :: MemField mode GPU.Int #{offset GPU_Attribute, location}
  , values   :: MemField mode (Ptr Void) #{offset GPU_Attribute, values}
  , format   :: MemField mode AttributeFormat #{offset GPU_Attribute, format}
  }

deriving stock instance Generic (AttributeF mode)
type Attribute = AttributeF HaskellMem
deriving stock instance Show Attribute
deriving via (MemStorable Attribute) instance Storable Attribute
instance MemData Attribute where
  memDataSize = #{size GPU_Attribute}
  memDataAlignment = #{alignment GPU_Attribute}

data AttributeSourceF mode = AttributeSource
  { enabled    :: MemField mode GPU.Bool    #{offset GPU_AttributeSource, enabled}
  , num_values :: MemField mode GPU.Int #{offset GPU_AttributeSource, num_values}
  , next_value :: MemField mode (Ptr Void)    #{offset GPU_AttributeSource, next_value}
  , per_vertex_storage_stride_bytes :: MemField mode GPU.Int    #{offset GPU_AttributeSource, per_vertex_storage_stride_bytes}
  , per_vertex_storage_offset_bytes :: MemField mode GPU.Int    #{offset GPU_AttributeSource, per_vertex_storage_offset_bytes}
  , per_vertex_storage_size :: MemField mode GPU.Int    #{offset GPU_AttributeSource, per_vertex_storage_size}
  , per_vertex_storage :: MemField mode (Ptr Void)    #{offset GPU_AttributeSource, per_vertex_storage}
  , attribute :: MemField mode Attribute #{offset GPU_AttributeSource, attribute}
  }

deriving stock instance Generic (AttributeSourceF mode)
type AttributeSource = AttributeSourceF HaskellMem
deriving stock instance Show AttributeSource
deriving via (MemStorable AttributeSource) instance Storable AttributeSource
instance MemData AttributeSource where
  memDataSize = #{size GPU_AttributeSource}
  memDataAlignment = #{alignment GPU_AttributeSource}

newtype ShaderEnum = ShaderEnum #{type GPU_ShaderEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

vertexShader :: ShaderEnum
vertexShader = #{const GPU_VERTEX_SHADER}

fragmentShader :: ShaderEnum
fragmentShader = #{const GPU_FRAGMENT_SHADER}

pixelShader :: ShaderEnum
pixelShader = #{const GPU_PIXEL_SHADER}

geometryShader :: ShaderEnum
geometryShader = #{const GPU_GEOMETRY_SHADER}

newtype ShaderLanguageEnum = ShaderLanguageEnum #{type GPU_ShaderLanguageEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

languageNone :: ShaderLanguageEnum
languageNone = #{const GPU_LANGUAGE_NONE}

languageARBAssembly :: ShaderLanguageEnum
languageARBAssembly = #{const GPU_LANGUAGE_ARB_ASSEMBLY}

languageGLSL :: ShaderLanguageEnum
languageGLSL = #{const GPU_LANGUAGE_GLSL}

languageGLSLES :: ShaderLanguageEnum
languageGLSLES = #{const GPU_LANGUAGE_GLSLES}

languageHLSL :: ShaderLanguageEnum
languageHLSL = #{const GPU_LANGUAGE_HLSL}

languageCG :: ShaderLanguageEnum
languageCG = #{const GPU_LANGUAGE_CG}

newtype ShaderProgram = ShaderProgram GPU.Uint32
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

newtype Shader = Shader GPU.Uint32
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

newtype TypeEnum = TypeEnum #{type GPU_TypeEnum}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

