module SDL.GPU.C.Rendering where

import Data.Word
import Data.Void
import Foreign.Ptr

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_Clear" clear
  :: Ptr GPU.Target
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_ClearColor" clearColor
  :: Ptr GPU.Target
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_ClearRGB" clearRGB
  :: Ptr GPU.Target
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_ClearRGBA" clearRGBA
  :: Ptr GPU.Target
  -> GPU.Uint8 -- ^ r
  -> GPU.Uint8 -- ^ g
  -> GPU.Uint8 -- ^ b
  -> GPU.Uint8 -- ^ a
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Blit" blit
  :: Ptr GPU.Image
  -> Ptr GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_BlitRotate" blitRotate
  :: Ptr GPU.Image
  -> Ptr GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ degrees
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_BlitScale" blitScale
  :: Ptr GPU.Image
  -> Ptr GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ scaleX
  -> GPU.Float -- ^ scaleY
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_BlitTransform" blitTransform
  :: Ptr GPU.Image
  -> Ptr GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ scaleX
  -> GPU.Float -- ^ scaleY
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_BlitTransformX" blitTransformX
  :: Ptr GPU.Image
  -> Ptr GPU.Rect
  -> Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ pivot_x
  -> GPU.Float -- ^ pivot_y
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ scaleX
  -> GPU.Float -- ^ scaleY
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_BlitRect" blitRect
  :: Ptr GPU.Image
  -> Ptr GPU.Rect -- ^ src_rect
  -> Ptr GPU.Target
  -> Ptr GPU.Rect -- ^ dest_rect
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_BlitRect" blitRectX
  :: Ptr GPU.Image
  -> Ptr GPU.Rect -- ^ src_rect
  -> Ptr GPU.Target
  -> Ptr GPU.Rect -- ^ dest_rect
  -> GPU.Float -- ^ degrees
  -> GPU.Float -- ^ pivot_x
  -> GPU.Float -- ^ pivot_y
  -> GPU.FlipEnum -- ^ flip_direction
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_TriangleBatch" triangleBatch
  :: Ptr GPU.Image
  -> Ptr GPU.Target
  -> GPU.UnsignedShort -- ^ num_vertices
  -> Ptr GPU.Float -- ^ values
  -> GPU.UnsignedInt -- ^ num_indices
  -> Ptr GPU.UnsignedShort -- ^ indices
  -> GPU.BatchFlagEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_TriangleBatchX" triangleBatchX
  :: Ptr GPU.Image
  -> Ptr GPU.Target
  -> GPU.UnsignedShort -- ^ num_vertices
  -> Ptr Void -- ^ values
  -> GPU.UnsignedInt -- ^ num_indices
  -> Ptr GPU.UnsignedShort -- ^ indices
  -> GPU.BatchFlagEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_PrimitiveBatch" primitiveBatch
  :: Ptr GPU.Image
  -> Ptr GPU.Target
  -> GPU.PrimitiveEnum -- ^ primitive_type
  -> GPU.UnsignedShort -- ^ num_vertices
  -> Ptr GPU.Float -- ^ values
  -> GPU.UnsignedInt -- ^ num_indices
  -> Ptr GPU.UnsignedShort -- ^ indices
  -> GPU.BatchFlagEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_PrimitiveBatchV" primitiveBatchV
  :: Ptr GPU.Image
  -> Ptr GPU.Target
  -> GPU.PrimitiveEnum -- ^ primitive_type
  -> GPU.UnsignedShort -- ^ num_vertices
  -> Ptr Void -- ^ values
  -> GPU.UnsignedInt -- ^ num_indices
  -> Ptr GPU.UnsignedShort -- ^ indices
  -> GPU.BatchFlagEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_FlushBlitBuffer" flushBlitBuffer
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_Flip" flip
  :: Ptr GPU.Target
  -> IO ()
