module SDL.GPU.C.RendererControls where

import Data.Int
import Data.Word
import Foreign.Ptr

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU


foreign import ccall safe "SDL_gpu.h GPU_ReserveNextRendererEnum" reserveNextRendererEnum
  :: IO GPU.RendererEnum

foreign import ccall safe "SDL_gpu.h GPU_GetNumActiveRenderers" getNumActiveRenderers
  :: IO GPU.Int

foreign import ccall safe "SDL_gpu.h GPU_GetActiveRendererList" getActiveRendererList
  :: Ptr GPU.RendererID -- ^ renderers_array
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetCurrentRenderer" getCurrentRenderer
  :: IO (Ptr GPU.Renderer)

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SetCurrentRenderer" setCurrentRenderer
  :: Ptr GPU.RendererID
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetRenderer" getRenderer
  :: Ptr GPU.RendererID
  -> IO (Ptr GPU.Renderer)

foreign import ccall safe "SDL_gpu.h GPU_FreeRenderer" freeRenderer
  :: Ptr GPU.Renderer
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_ResetRendererState" resetRendererState
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetCoordinateMode" setCoordinateMode
  :: GPU.Bool -- ^ use_math_coords
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetCoordinateMode" getCoordinateMode
  :: IO (GPU.Bool) -- ^ use_math_coords

foreign import ccall safe "SDL_gpu.h GPU_SetDefaultAnchor" setDefaultAnchor
  :: GPU.Float -- ^ anchor_x
  -> GPU.Float -- ^ anchor_y
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetDefaultAnchor" getDefaultAnchor
  :: Ptr GPU.Float -- ^ anchor_x
  -> Ptr GPU.Float -- ^ anchor_y
  -> IO ()


