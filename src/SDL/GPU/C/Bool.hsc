{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module SDL.GPU.C.Bool where

import Prelude hiding (Bool)
import qualified Prelude

import Data.Bits
import Data.Word
import Foreign.Storable

#include <SDL_gpu.h>

newtype Bool = Bool #{type GPU_bool}
  deriving stock (Eq, Show)
  deriving newtype (Num, Bits, Storable)

true, false :: Bool
true = 1
false = 0

isTrue :: Bool -> Prelude.Bool
isTrue b = if (b == 0) then Prelude.False else Prelude.True

bool :: Prelude.Bool -> Bool
bool = \case
  True -> true
  False -> false

