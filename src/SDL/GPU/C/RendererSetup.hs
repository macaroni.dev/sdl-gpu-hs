module SDL.GPU.C.RendererSetup where

import Data.Int
import Data.Word
import Foreign.Ptr
import Foreign.C.String

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_Adapter_MakeRendererID" makeRendererID
  :: Ptr GPU.RendererID -- ^ out
  -> CString -- ^ name
  -> GPU.RendererEnum
  -> GPU.Int -- ^ major_version
  -> GPU.Int -- ^ minor_version
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_GetRendererID" getRendererID
  :: Ptr GPU.RendererID -- ^ out
  -> GPU.RendererEnum
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetNumRegisteredRenderers" getNumRegisteredRenderers
  :: IO GPU.Int

foreign import ccall safe "SDL_gpu.h GPU_GetRegisteredRendererList" getRegisteredRendererList
  :: Ptr GPU.RendererID -- ^ renderers_array
  -> IO ()

-- NOTE: NOT BOUND - GPU_RegisterRenderer
