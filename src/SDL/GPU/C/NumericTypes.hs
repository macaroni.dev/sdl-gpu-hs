{-# LANGUAGE NoImplicitPrelude #-}

module SDL.GPU.C.NumericTypes where

import SDL.GPU.C.NumericTypes.Inner

type Int = GPU_Int
type Float = GPU_Float
type UnsignedInt = GPU_UnsignedInt
type UnsignedShort = GPU_UnsignedShort
type UnsignedChar = GPU_UnsignedChar
type Sint16 = GPU_Sint16
type Uint8 = GPU_Uint8
type Uint16 = GPU_Uint16
type Uint32 = GPU_Uint32
