module SDL.GPU.C.Initialization where

import Data.Int
import Data.Word
import Foreign.C.Types
import Foreign.Ptr

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

#include <SDL_gpu.h>

foreign import ccall safe "SDL_gpu.h GPU_SetInitWindow" setInitWindow
  :: Word32 -- ^ windowID
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetInitWindow" getInitWindow
  :: IO Word32

foreign import ccall safe "SDL_gpu.h GPU_GetLinkedVersion" getLinkedVersion
  :: Ptr SDL.Version
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetPreInitFlags" setPreInitFlags
  :: GPU.InitFlagEnum -- ^ GPU_flags
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetPreInitFlags" getPreInitFlags
  :: IO GPU.InitFlagEnum

foreign import ccall safe "SDL_gpu.h GPU_SetRequiredFeatures" setRequiredFeatures
  :: GPU.FeatureEnum -- ^ GPU_flags
  -> IO ()

rendererOrderMax :: Int
rendererOrderMax = #{const GPU_RENDERER_ORDER_MAX}

foreign import ccall safe "SDL_gpu.h GPU_GetRequiredFeatures" getRequiredFeatures
  :: IO GPU.FeatureEnum

foreign import ccall safe "SDL_gpu.h GPU_GetDefaultRendererOrder" getDefaultRendererOrder
  :: Ptr Int -- ^ order_size
  -> Ptr GPU.RendererID -- ^ order
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_GetRendererOrder" getRendererOrder
  :: Ptr Int -- ^ order_size
  -> Ptr GPU.RendererID -- ^ order
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_SetRendererOrder" setRendererOrder
  :: Int -- ^ order_size
  -> Ptr GPU.RendererID -- ^ order
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Init" init
  :: Word16 -- ^ w
  -> Word16 -- ^ h
  -> GPU.WindowFlagEnum
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_InitRenderer" initRenderer
  :: GPU.RendererEnum
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> GPU.WindowFlagEnum
  -> IO (Ptr GPU.Target)

foreign import ccall safe "GPU_Adapter_InitRendererByID" initRendererByID
  :: Ptr GPU.RendererID
  -> GPU.Uint16 -- ^ w
  -> GPU.Uint16 -- ^ h
  -> GPU.WindowFlagEnum
  -> IO (Ptr GPU.Target)

foreign import ccall safe "SDL_gpu.h GPU_IsFeatureEnabled" isFeatureEnabled
  :: GPU.FeatureEnum
  -> IO GPU.Bool

foreign import ccall safe "SDL_gpu.h GPU_CloseCurrentRenderer" closeCurrentRenderer
  :: IO ()

foreign import ccall safe "SDL_gpu.h GPU_Quit" quit
  :: IO ()
