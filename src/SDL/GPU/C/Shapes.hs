module SDL.GPU.C.Shapes where

import Data.Word
import Foreign.Ptr

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU
import qualified SDL.GPU.C.Bool as GPU
import qualified SDL.GPU.C.NumericTypes as GPU

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Pixel" pixel
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Line" line
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Arc" arc
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_ArcFilled" arcFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Circle" circle
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_CircleFilled" circleFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ radius
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Ellipse" ellipse
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ rx
  -> GPU.Float -- ^ ry
  -> GPU.Float -- ^ degrees
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_EllipseFilled" ellipseFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ rx
  -> GPU.Float -- ^ ry
  -> GPU.Float -- ^ degrees
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Sector" sector
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ inner_radius
  -> GPU.Float -- ^ outer_radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_SectorFilled" sectorFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x
  -> GPU.Float -- ^ y
  -> GPU.Float -- ^ inner_radius
  -> GPU.Float -- ^ outer_radius
  -> GPU.Float -- ^ start_angle
  -> GPU.Float -- ^ end_angle
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Tri" tri
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ x3
  -> GPU.Float -- ^ y3
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_TriFilled" triFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ x3
  -> GPU.Float -- ^ y3
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Rectangle" rectangle
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Rectangle2" rectangle2
  :: Ptr GPU.Target
  -> Ptr GPU.Rect
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_RectangleFilled" rectangleFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_RectangleFilled2" rectangleFilled2
  :: Ptr GPU.Target
  -> Ptr GPU.Rect
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_RectangleRound" rectangleRound
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ radius
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_RectangleRound2" rectangleRound2
  :: Ptr GPU.Target
  -> Ptr GPU.Rect
  -> GPU.Float -- ^ radius
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_RectangleRoundFilled" rectangleRoundFilled
  :: Ptr GPU.Target
  -> GPU.Float -- ^ x1
  -> GPU.Float -- ^ y1
  -> GPU.Float -- ^ x2
  -> GPU.Float -- ^ y2
  -> GPU.Float -- ^ radius
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_RectangleRoundFilled2" rectangleRoundFilled2
  :: Ptr GPU.Target
  -> Ptr GPU.Rect
  -> GPU.Float -- ^ radius
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Polygon" polygon
  :: Ptr GPU.Target
  -> GPU.UnsignedInt -- ^ num_vertices
  -> Ptr GPU.Float -- ^ vertices
  -> Ptr SDL.Color
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_Polyline" polyline
  :: Ptr GPU.Target
  -> GPU.UnsignedInt -- ^ num_vertices
  -> Ptr GPU.Float -- ^ vertices
  -> Ptr SDL.Color
  -> GPU.Bool -- ^ close_loop
  -> IO ()

foreign import ccall safe "SDL_gpu.h GPU_Adapter_PolygonFilled" polygonFilled
  :: Ptr GPU.Target
  -> GPU.UnsignedInt -- ^ num_vertices
  -> Ptr GPU.Float -- ^ vertices
  -> Ptr SDL.Color
  -> IO ()
