module SDL.GPU.C.Conversions where

import Data.Word
import Foreign.Ptr

import qualified SDL.Raw.Types as SDL

import qualified SDL.GPU.C.Types as GPU

foreign import ccall safe "SDL_gpu.h GPU_CopyImageFromSurface" copyImageFromSurface
  :: Ptr SDL.Surface
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_CopyImageFromTarget" copyImageFromTarget
  :: Ptr GPU.Target
  -> IO (Ptr GPU.Image)

foreign import ccall safe "SDL_gpu.h GPU_CopySurfaceFromTarget" copySurfaceFromTarget
  :: Ptr GPU.Target
  -> IO (Ptr SDL.Surface)

foreign import ccall safe "SDL_gpu.h GPU_CopySurfaceFromImage" copySurfaceFromImage
  :: Ptr GPU.Image
  -> IO (Ptr SDL.Surface)
