{-# LANGUAGE StrictData #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NegativeLiterals #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import qualified SDL
import qualified SDL.GPU.C as GPU
import qualified SDL.GPU.C as Rect (RectF(..))
import qualified SDL.GPU.C as Target (TargetF(..))
import qualified SDL.GPU.FC.C as FC
import qualified SDL.Raw.Types as SDLRaw

import Control.Monad (when, unless, join)
import Data.IORef
import Data.Functor ((<&>))
import Data.Function ((&))
import Foreign.C.Types
import Foreign.Storable
import Foreign.Ptr (Ptr, nullPtr)
import Foreign.C.String (withCString)
import System.Exit (die)

import Memorable

------------------------------------------------------------------
-- START

data Ship = Ship
  { image :: Ptr GPU.Image
  -- Motion
  , pos_x :: Pinned Float
  , pos_y :: Pinned Float
  , vel_x :: Pinned Float
  , vel_y :: Pinned Float
  , angle :: Pinned Float
  -- Properties of the ship
  , thrust :: Pinned Float
  , drag_coefficient :: Pinned Float
  }

print_ship :: Ship -> IO ()
print_ship Ship{..} = do
  px <- val pos_x
  py <- val pos_y
  vx <- val vel_x
  vy <- val vel_y
  a <- val angle
  t <- val thrust
  dc <- val drag_coefficient
  putStrLn $ mconcat
    [ "pos_x = ", show px, "; "
    , "pos_y = ", show py, "; "
    , "vel_x = ", show vx, "; "
    , "vel_y = ", show vy, "; "
    , "angle = ", show a, "; "
    , "thrust = ", show t, "; "
    , "drag_coefficient = ", show dc
    ]

create_ship :: FilePath -> IO (Ship)
create_ship imgPath = do
  image <- withCString imgPath $ \cpath -> GPU.loadImage cpath
  pos_x <- var 0.0
  pos_y <- var 0.0
  vel_x <- var 0.0
  vel_y <- var 0.0
  angle <- var 0.0
  thrust <- var 50.0
  drag_coefficient <- var 0.0005
  pure Ship{..}

free_ship :: Ship -> IO ()
free_ship Ship{..} = GPU.freeImage image

apply_thrust :: Ship -> Float -> IO ()
apply_thrust Ship{..} dt = do
  withPeek thrust $ \t -> withPeek angle $ \a -> do
    vel_x += t * cos a * dt
    vel_y += t * sin a * dt

apply_drag :: Ship -> Float -> IO ()
apply_drag Ship{..} dt = do
  withPeek vel_x $ \vx -> withPeek vel_y $ \vy -> do
    let vel_angle = atan2 vy vx
    vel <- var $ sqrt (vx * vx + vy * vy)
    withPeek vel $ \v -> withPeek drag_coefficient $ \dc -> vel -= dc * v * v
    withPeek vel $ \v -> when (v < 0) $ vel .= 0

    withPeek vel $ \v -> do
      vel_x .= v * cos vel_angle
      vel_y .= v * sin vel_angle

update_ship :: PtrLike ptr => Ship -> ptr GPU.Rect -> Float -> IO ()
update_ship Ship{..} play_area_like dt = withPtr play_area_like $ \play_area -> do
  withPeek vel_x $ \n -> pos_x += n + dt
  withPeek vel_y $ \n -> pos_y += n + dt

  withPeek pos_x $ \x ->
    withPeek (play_area &-> Rect.x) $ \rect_x ->
    withPeek (play_area &-> Rect.w) $ \rect_w ->
    if (x < rect_x) then do
      pos_x .= rect_x
      vel_x %= (* -1)
    else when (x >= rect_x + rect_w) $ do
      pos_x .= rect_x + rect_w
      vel_x %= (* -1)

  withPeek pos_y $ \y ->
    withPeek (play_area &-> Rect.y) $ \rect_y ->
    withPeek (play_area &-> Rect.h) $ \rect_h ->
    if (y < rect_y) then do
      pos_y .= rect_y
      vel_y %= (* -1)
    else when (y >= rect_y + rect_h) $ do
      pos_y .= rect_y + rect_h
      vel_y %= (* -1)

draw_ship :: Ship -> Ptr GPU.Target -> IO ()
draw_ship ship@Ship{..} screen = do
  let renderShip =
            join $ GPU.blitRotate image nullPtr screen
        <$> (val pos_x)
        <*> (val pos_y)
        <*> (val angle <&> \a -> a * 180 / pi)
  renderShip

main :: IO ()
main = do
  GPU.setDebugLevel GPU.debugLevelMax

  screen <- GPU.init 800 600 GPU.defaultInitFlags

  when (screen == nullPtr) $ die "Failed to GPU.init"

  freeSans <- FC.createFont
  alagard <- FC.createFont

  whiteColor <- var (SDLRaw.Color 255 255 255 255)

  ret <-
    withCString "FreeSans.ttf" $ \path ->
    withPtr whiteColor $ \whitePtr ->
      FC.loadFont freeSans path 20 whitePtr 0

  unless (GPU.isTrue ret) $ die "Failed to load FreeSans.ttf"

  ret <-
    withCString "alagard.ttf" $ \path ->
    withPtr whiteColor $ \whitePtr ->
      FC.loadFont alagard path 20 whitePtr 0

  unless (GPU.isTrue ret) $ die "Failed to load alagard.ttf"

  -- Game variables
  play_area <- do
    w <- fromIntegral <$> screen *-> Target.w
    h <- fromIntegral <$> screen *-> Target.h
    rect <- newPinned
    rect & Rect.w *->= w
    rect & Rect.h *->= h
    pure rect

  player_ship <- create_ship "ship.png"

  do
    let Ship{..} = player_ship
    x <- play_area *-> Rect.x
    y <- play_area *-> Rect.y
    w <- play_area *-> Rect.y
    h <- play_area *-> Rect.y
    pos_x .= (x + w/2)
    pos_y .= (y + h/2)

  done <- var False
  dt <- var @Float 0.0
  start_time <- SDL.ticks >>= var

  while (not <$> val done) $ do
    -- Check events

    let handleEvent e = case e of
          SDL.QuitEvent -> done .= True
          SDL.KeyboardEvent (SDL.KeyboardEventData{..}) -> do
            when (keyboardEventKeyMotion == SDL.Pressed) $
              case SDL.keysymKeycode keyboardEventKeysym of
                SDL.KeycodeEscape -> done .= True
                _ -> pure ()
          _ -> pure ()
    SDL.pollEvents >>= traverse (handleEvent . SDL.eventPayload)

    -- Update
    mouse_pressed <- SDL.getMouseButtons
    SDL.P (SDL.V2 mouse_x mouse_y) <- SDL.getAbsoluteMouseLocation
    apply_drag player_ship =<< val dt

    do
      let Ship{..} = player_ship
      ship_y <- val pos_y
      ship_x <- val pos_x
      angle .= atan2 (fromIntegral mouse_y - ship_y) (fromIntegral mouse_x - ship_x)

    when (mouse_pressed SDL.ButtonLeft) $
      apply_thrust player_ship =<< val dt

    update_ship player_ship play_area =<< val dt

    --print_ship player_ship
    -- Draw
    GPU.clear screen
    draw_ship player_ship screen
    withCString "Hello, FreeSans" $ \s -> FC.draw nullPtr freeSans screen 10 10 s
    withCString "Hello, Alagard" $ \s -> FC.draw nullPtr alagard screen 10 300 s
    GPU.flip screen

    -- Timing
    SDL.delay 10
    end_time <- SDL.ticks
    withPeek start_time $ \st -> dt .= (fromIntegral (end_time - st))/(1000 :: Float)
    start_time .= end_time

  free_ship player_ship
  FC.freeFont freeSans
  FC.freeFont alagard
  GPU.quit
