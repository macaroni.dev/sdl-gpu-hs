{-# LANGUAGE StrictData #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NegativeLiterals #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedRecordDot #-}

module Main where

import qualified SDL
import qualified SDL.GPU.Simple as GPU
import qualified SDL.GPU.Simple as Rect (RectF(..))
import qualified SDL.GPU.Simple as Target (TargetF(..))
import qualified SDL.GPU.FC.Simple as FC
import qualified SDL.Raw.Types as SDLRaw

import Control.Monad (when, unless, join)
import Data.IORef
import Data.Word
import Data.List (sortOn)
import Data.Traversable (for)
import Data.Foldable (for_)
import Data.Functor ((<&>))
import Data.Function ((&), fix)
import Foreign.C.Types
import Linear
import Foreign.Marshal.Alloc
import Foreign.Storable
import Foreign.Ptr (Ptr, nullPtr)
import Foreign.C.String (withCString)
import System.Exit (die)
import System.Random.TF
import qualified  System.Random.TF.Gen as TF
import qualified System.Random.TF.Instances as TF
import Control.Monad.State.Strict as State

import Memorable

------------------------------------------------------------------
_TODO :: a
_TODO = error "TODO"


------------------------------------------------------------------
-- START

data Ship = Ship
  { image :: Ptr GPU.Image
  -- Motion
  , pos_x :: Pinned Float
  , pos_y :: Pinned Float
  , vel_x :: Pinned Float
  , vel_y :: Pinned Float
  , angle :: Pinned Float
  -- Properties of the ship
  , thrust :: Pinned Float
  , drag_coefficient :: Pinned Float
  }

data Starfield = Starfield [Star] Float deriving (Eq, Show)
data Star = Star Float Float deriving (Eq, Show)

data Shipwreck = Shipwreck Float Float Float deriving (Eq, Show)

type Seed = (Word64, Word64, Word64,Word64)

rndR :: TF.Random a => (a, a) -> State TFGen a
rndR range = do
  g <- State.get
  let (a, g') = TF.randomR range g
  State.put g'
  pure a

chooseR :: [a] -> State TFGen a
chooseR choices = do
  when (null choices) $ error "chooseR needs choices"
  choice <- rndR (0, length choices - 1)
  pure $ choices !! choice

gen_starfields
  :: GPU.Rect -- ^ Playing area
  -> [(Float, Int)] -- ^ layer_depths
  -> TFGen
  -> IO [Starfield]
gen_starfields GPU.Rect{..} layers gen = do
  pure $ flip evalState gen $ for layers $ \(z, stars_per_layer) -> do
    stars <- replicateM stars_per_layer $ do
      x :: Int <- rndR (0, round w)
      y :: Int <- rndR (0, round h)

      pure $ Star (fromIntegral x) (fromIntegral y)

    pure $ Starfield (Star 0 0 : stars) z

gen_shipwrecks
  :: GPU.Rect
  -> Int -- ^ num_shipwrecks
  -> TFGen
  -> IO [Shipwreck]
gen_shipwrecks GPU.Rect{..} num_shipwrecks gen = do
  let depths = [-5.0, -50.0, -100.0, -200.0]
  pure $ flip evalState gen $ replicateM num_shipwrecks $ do
      x :: Int <- rndR (0, round w)
      y :: Int <- rndR (0, round h)
      z <- chooseR depths
      pure $ Shipwreck (fromIntegral x) (fromIntegral y) z



print_ship :: Ship -> IO ()
print_ship Ship{..} = do
  px <- val pos_x
  py <- val pos_y
  vx <- val vel_x
  vy <- val vel_y
  a <- val angle
  t <- val thrust
  dc <- val drag_coefficient
  putStrLn $ mconcat
    [ "pos_x = ", show px, "; "
    , "pos_y = ", show py, "; "
    , "vel_x = ", show vx, "; "
    , "vel_y = ", show vy, "; "
    , "angle = ", show a, "; "
    , "thrust = ", show t, "; "
    , "drag_coefficient = ", show dc
    ]

create_ship :: FilePath -> IO (Ship)
create_ship imgPath = do
  image <- GPU.loadImage imgPath
  GPU.setImageFilter image GPU.filterNearest
  pos_x <- var 0.0
  pos_y <- var 0.0
  vel_x <- var 0.0
  vel_y <- var 0.0
  angle <- var 0.0
  thrust <- var 500.0
  drag_coefficient <- var 0.00005
  pure Ship{..}

free_ship :: Ship -> IO ()
free_ship Ship{..} = GPU.freeImage image

apply_thrust :: Ship -> Float -> IO ()
apply_thrust Ship{..} dt = do
  withPeek thrust $ \t -> withPeek angle $ \a -> do
    vel_x += t * cos a * dt
    vel_y += t * sin a * dt

apply_drag :: Ship -> Float -> IO ()
apply_drag Ship{..} dt = do
  withPeek vel_x $ \vx -> withPeek vel_y $ \vy -> do
    let vel_angle = atan2 vy vx
    vel <- var $ sqrt (vx * vx + vy * vy)
    withPeek vel $ \v -> withPeek drag_coefficient $ \dc -> vel -= dc * v * v
    withPeek vel $ \v -> when (v < 0) $ vel .= 0

    withPeek vel $ \v -> do
      vel_x .= v * cos vel_angle
      vel_y .= v * sin vel_angle

apply_brake :: Ship -> Float -> IO ()
apply_brake Ship{..} dt = do
  vel_x .= 0
  vel_y .= 0

update_ship :: Ship -> GPU.Rect -> Float -> IO ()
update_ship Ship{..} play_area dt = do
  withPeek vel_x $ \n -> pos_x += n * dt
  withPeek vel_y $ \n -> pos_y += n * dt

  withPeek pos_x $ \x ->
    if (x < play_area.x) then do
      pos_x .= play_area.x
      vel_x %= (* -1)
    else when (x >= play_area.x + play_area.w) $ do
      pos_x .= play_area.x + play_area.w
      vel_x %= (* -1)

  withPeek pos_y $ \y ->
    if (y < play_area.y) then do
      pos_y .= play_area.y
      vel_y %= (* -1)
    else when (y >= play_area.y + play_area.h) $ do
      pos_y .= play_area.y + play_area.h
      vel_y %= (* -1)

draw_ship :: Ship -> Ptr GPU.Target -> IO ()
draw_ship ship@Ship{..} screen = do
  let renderShip =
            join $ GPU.blitRotate image Nothing screen
        <$> (val pos_x)
        <*> (val pos_y)
        <*> (val angle <&> \a -> a * 180 / pi)
  renderShip

draw_background :: Ship -> GPU.Color -> Ptr GPU.Image -> [Starfield] -> Ptr GPU.Image -> [Shipwreck] -> Ptr GPU.Target -> IO ()
draw_background ship anchorColor star_image starfields wreck_image wrecks screen = do
  for_ starfields $ \(Starfield stars z) -> do
    GPU.withFreshMatrix screen GPU.projection $ do
      GPU.perspective 90 (800 / 600) 0.1 2000

      GPU.withFreshMatrix screen GPU.view $ do
        let Ship{..} = ship
        x <- val pos_x
        y <- val pos_y

        -- We're in perspective, so where we look will be the center of the screen
        GPU.lookAt
          x
          y
          300
          x
          y
          0
          0
          1
          0

        GPU.withFreshMatrix screen GPU.model $ do
          GPU.translate 0 0 z

          GPU.withAliasImage star_image $ \anchorStar -> do
            GPU.setColor anchorStar anchorColor

            for_ stars $ \(Star x y) -> do
              GPU.blit (if x == 0 && y == 0 then toPtr anchorStar else star_image) Nothing screen x y

        -- Wrecks
        -- We sort these by z coord to ensure correct draw order
        -- (Since we aren't depth testing)
        let sortedWrecks = sortOn (\(Shipwreck _ _ z) -> z) wrecks

        for_ sortedWrecks $ \(Shipwreck x y z) -> do
          GPU.withFreshMatrix screen GPU.model $ do
            GPU.translate 0 0 z
            GPU.blit wreck_image Nothing screen x y

draw_camera :: Ship -> Ptr GPU.Target -> IO ()
draw_camera Ship{..} screen = do
  x <- val pos_x
  y <- val pos_y

  screen_w <- screen *-> Target.w
  screen_h <- screen *-> Target.h

  -- We're in ortho, so where we look will be the origin (top-left) of the screen
  let look_x = x - (fromIntegral screen_w/2)
  let look_y = y - (fromIntegral screen_h/2)
  GPU.lookAt
    look_x
    look_y
    500--300
    look_x
    look_y
    0
    0
    1
    0

with_camera :: Ship -> Ptr GPU.Target -> IO r -> IO r
with_camera Ship{..} screen k = GPU.withFreshMatrix screen GPU.projection $ do
  GPU.ortho 0 800 600 0 (-1000) 1000
  GPU.withFreshMatrix screen GPU.view $ do
    x <- val pos_x
    y <- val pos_y

    screen_w <- screen *-> Target.w
    screen_h <- screen *-> Target.h

    -- We're in ortho, so where we look will be the origin (top-left) of the screen
    let look_x = x - (fromIntegral screen_w/2)
    let look_y = y - (fromIntegral screen_h/2)
    GPU.lookAt
      look_x
      look_y
      500--300
      look_x
      look_y
      0
      0
      1
      0
    k

main :: IO ()
main = do
  GPU.setDebugLevel GPU.debugLevelMax

  screen <- GPU.init 800 600 GPU.defaultInitFlags

-- TODO: How to get transparency to work? Maybe some custom blend func?
-- If so, give it a name and add it to the library
--  GPU.setDepthTest screen 1
--  GPU.addDepthBuffer screen >>= \success -> unless (GPU.isTrue success) $ die "Failed to GPU.addDepthBuffer"
  GPU.enableCamera screen False

  when (screen == nullPtr) $ die "Failed to GPU.init"

  let whiteColor = GPU.Color 255 255 255 255
  let orangeColor = GPU.Color 255 155 79 255

  freeSans <-
    FC.loadFont "FreeSans.ttf" 20 whiteColor 0

  alagard <-
    FC.loadFont "alagard.ttf" 20 whiteColor 0

  FC.setFilterMode alagard FC.filterNearest

  minimapRect <- initPinned $ \r -> do
    r & Rect.x *->= 450
    r & Rect.y *->= 600
    r & Rect.w *->= 200
    r & Rect.h *->= 150

  -- Game variables
  play_area <- do
    w <- fromIntegral . (* 10) <$> screen *-> Target.w
    h <- fromIntegral . (* 10) <$> screen *-> Target.h
    pure GPU.Rect
        { x = 0
        , y = 0
        , ..
        }

  player_ship@Ship{image=ship_image} <- create_ship "ship.png"

  seed <- mkSeedUnix
  let gen = seedTFGen seed
  let (starGen, wreckGen) = TF.split gen

  star_image <- GPU.loadImage "star.png"
  starfields <- gen_starfields play_area [(-400.0, 5000), (-700.0, 10000)]  starGen

  wreck_image <- GPU.loadImage "shipwreck.png"
  wrecks <- gen_shipwrecks play_area 250 wreckGen

  do
    let Ship{..} = player_ship
    pos_x .= 0--(play_area.x + play_area.w/2)
    pos_y .= 0--(play_area.y + play_area.h/2)

  done <- var False
  dt <- var @Float 0.0
  start_time <- SDL.ticks >>= var

  while (not <$> val done) $ do
    -- Check events

    let handleEvent e = case e of
          SDL.QuitEvent -> done .= True
          SDL.KeyboardEvent (SDL.KeyboardEventData{..}) -> do
            when (keyboardEventKeyMotion == SDL.Pressed) $
              case SDL.keysymKeycode keyboardEventKeysym of
                SDL.KeycodeEscape -> done .= True
                _ -> pure ()
          _ -> pure ()
    SDL.pollEvents >>= traverse (handleEvent . SDL.eventPayload)

    -- Update
    mouse_pressed <- SDL.getMouseButtons
    SDL.P (SDL.V2 mouse_x mouse_y) <- SDL.getAbsoluteMouseLocation
    --apply_drag player_ship =<< val dt

    V2 virt_x virt_y <-
      GPU.getVirtualCoords screen $ V2 (fromIntegral mouse_x) (fromIntegral mouse_y)

    --putStrLn $ "mouse_x = " ++ show mouse_x ++ "; mouse_y = " ++ show mouse_y ++ "; virt_x = " ++ show virt_x ++ "; virt_y = " ++ show virt_y

    do
      let Ship{..} = player_ship
      screen_w <- fromIntegral . (* 10) <$> screen *-> Target.base_w
      screen_h <- fromIntegral . (* 10) <$> screen *-> Target.base_h
      --putStrLn $ unwords [ "screen_w =", show screen_w, "screen_h =", show screen_h ]
      angle .= atan2 (fromIntegral mouse_y - (screen_h/20)) (fromIntegral mouse_x - (screen_w/20))
      -- TODO: Why are w and h x10??


    when (mouse_pressed SDL.ButtonLeft) $
      apply_thrust player_ship =<< val dt

    when (mouse_pressed SDL.ButtonRight) $
      apply_brake player_ship =<< val dt

    update_ship player_ship play_area =<< val dt

    --print_ship player_ship
    -- Draw
    GPU.clear screen
    with_camera player_ship screen $ do
      draw_background player_ship orangeColor star_image starfields wreck_image wrecks screen
      FC.draw freeSans screen 10 10 "Hello, FreeSans"
      FC.draw alagard screen 10 300 "Hello, Alagard"
      draw_ship player_ship screen
      GPU.rectangle2 screen play_area orangeColor

    -- HUD
    -- Don't apply a View matrix at all!
    GPU.withFreshMatrix screen GPU.view $ do
      do
        let Ship{..} = player_ship
        x <- val pos_x
        y <- val pos_y
        FC.draw freeSans screen 10 10 (show (round x :: Int, round y :: Int))

    GPU.flip screen

    -- Timing
    SDL.delay 10
    end_time <- SDL.ticks
    withPeek start_time $ \st -> dt .= (fromIntegral (end_time - st))/(1000 :: Float)
    start_time .= end_time

  free_ship player_ship
  FC.freeFont freeSans
  FC.freeFont alagard
  GPU.quit

don't :: Applicative m => m a -> m ()
don't = const (pure ())
