#include <SDL_gpu.h>

void GPU_Adapter_SetColorPtr(GPU_Image* image, SDL_Color* color) {
  GPU_SetColor(image, *color);
}

void GPU_Adapter_ClearColor(GPU_Target* target, SDL_Color* color) {
  GPU_ClearColor(target, *color);
}

void GPU_Adapter_MakeRect(float x, float y, float w, float h, GPU_Rect* out) {
  *out = GPU_MakeRect(x, y, w, h);
}

void GPU_Adapter_MakeColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a, SDL_Color* out) {
  *out = GPU_MakeColor(r, g, b, a);
}

void GPU_Adapter_SetViewport(GPU_Target* target, GPU_Rect* viewport) {
  GPU_SetViewport(target, *viewport);
}

void GPU_Adapter_GetDefaultCamera(GPU_Camera* out) {
  *out = GPU_GetDefaultCamera();
}

void GPU_Adapter_GetCamera(GPU_Target* target, GPU_Camera* out) {
  *out = GPU_GetCamera(target);
}

void GPU_Adapter_SetCamera(GPU_Target* target, GPU_Camera* cam) {
  GPU_SetCamera(target, cam);
}

void GPU_Adapter_SetCameraReturning(GPU_Target* target, GPU_Camera* cam, GPU_Camera* out) {
  *out = GPU_SetCamera(target, cam);
}

void GPU_Adapter_GetPixel(GPU_Target* target, Sint16 x, Sint16 y, SDL_Color* out) {
  *out = GPU_GetPixel(target, x, y);
}

void GPU_Adapter_SetClipRect(GPU_Target* target, GPU_Rect* rect) {
  GPU_SetClipRect(target, *rect);
}

void GPU_Adapter_SetClipRectReturning(GPU_Target* target, GPU_Rect* rect, GPU_Rect* out) {
  *out = GPU_SetClipRect(target, *rect);
}

void GPU_Adapter_SetClipReturning(GPU_Target* target, Sint16 x, Sint16 y, Uint16 w, Uint16 h, GPU_Rect* out) {
  *out = GPU_SetClip(target, x, y, w, h);
}

GPU_bool  GPU_Adapter_IntersectRect(GPU_Rect* A, GPU_Rect* B, GPU_Rect* result) {
  return GPU_IntersectRect(*A, *B, result);
}

GPU_bool GPU_Adapter_IntersectClipRect(GPU_Target* target, GPU_Rect* B, GPU_Rect* result) {
  return GPU_IntersectClipRect(target, *B, result);
}

void GPU_Adapter_SetTargetColor(GPU_Target* target, SDL_Color* color) {
  GPU_SetTargetColor(target, *color);
}

// Shapes

void GPU_Adapter_Pixel (GPU_Target *target, float x, float y, SDL_Color* color) {
  GPU_Pixel(target, x, y, *color);
}
 
void GPU_Adapter_Line (GPU_Target *target, float x1, float y1, float x2, float y2, SDL_Color* color) {
  GPU_Line(target, x1, y1, x2, y2, *color);
}
 
void GPU_Adapter_Arc (GPU_Target *target, float x, float y, float radius, float start_angle, float end_angle, SDL_Color* color) {
  GPU_Arc(target, x, y, radius, start_angle, end_angle, *color);
}
 
void GPU_Adapter_ArcFilled (GPU_Target *target, float x, float y, float radius, float start_angle, float end_angle, SDL_Color* color) {
  GPU_ArcFilled(target, x, y, radius, start_angle, end_angle, *color);
}
 
void GPU_Adapter_Circle (GPU_Target *target, float x, float y, float radius, SDL_Color* color) {
  GPU_Circle(target, x, y, radius, *color);
}
 
void GPU_Adapter_CircleFilled (GPU_Target *target, float x, float y, float radius, SDL_Color* color) {
  GPU_CircleFilled(target, x, y, radius, *color);
}
 
void GPU_Adapter_Ellipse (GPU_Target *target, float x, float y, float rx, float ry, float degrees, SDL_Color* color) {
  GPU_Ellipse(target, x, y, rx, ry, degrees, *color);
}
 
void GPU_Adapter_EllipseFilled (GPU_Target *target, float x, float y, float rx, float ry, float degrees, SDL_Color* color) {
  GPU_EllipseFilled(target, x, y, rx, ry, degrees, *color);
}

void GPU_Adapter_Sector (GPU_Target *target, float x, float y, float inner_radius, float outer_radius, float start_angle, float end_angle, SDL_Color* color) {
  GPU_Sector(target, x, y, inner_radius, outer_radius, start_angle, end_angle, *color);
}
 
void GPU_Adapter_SectorFilled (GPU_Target *target, float x, float y, float inner_radius, float outer_radius, float start_angle, float end_angle, SDL_Color* color) {
  GPU_SectorFilled(target, x, y, inner_radius, outer_radius, start_angle, end_angle, *color);
}
 
void GPU_Adapter_Tri (GPU_Target *target, float x1, float y1, float x2, float y2, float x3, float y3, SDL_Color* color) {
  GPU_Tri(target, x1, y1, x2, y2, x3, y3, *color);
}
 
void GPU_Adapter_TriFilled (GPU_Target *target, float x1, float y1, float x2, float y2, float x3, float y3, SDL_Color* color) {
  GPU_TriFilled(target, x1, y1, x2, y2, x3, y3, *color);
}
 
void GPU_Adapter_Rectangle (GPU_Target *target, float x1, float y1, float x2, float y2, SDL_Color* color) {
  GPU_Rectangle(target, x1, y1, x2, y2, *color);
}
 
void GPU_Adapter_Rectangle2 (GPU_Target *target, GPU_Rect* rect, SDL_Color* color) {
  GPU_Rectangle2(target, *rect, *color);
}
 
void GPU_Adapter_RectangleFilled (GPU_Target *target, float x1, float y1, float x2, float y2, SDL_Color* color) {
  GPU_RectangleFilled(target, x1, y1, x2, y2, *color);
}
 
void GPU_Adapter_RectangleFilled2 (GPU_Target *target, GPU_Rect* rect, SDL_Color* color) {
  GPU_RectangleFilled2(target, *rect, *color);
}
 
void GPU_Adapter_RectangleRound (GPU_Target *target, float x1, float y1, float x2, float y2, float radius, SDL_Color* color) {
  GPU_RectangleRound(target, x1, y1, x2, y2, radius, *color);
}
 
void GPU_Adapter_RectangleRound2 (GPU_Target *target, GPU_Rect* rect, float radius, SDL_Color* color) {
  GPU_RectangleRound2(target, *rect, radius, *color);
}
 
void GPU_Adapter_RectangleRoundFilled (GPU_Target *target, float x1, float y1, float x2, float y2, float radius, SDL_Color* color) {
  GPU_RectangleRoundFilled(target, x1, y1, x2, y2, radius, *color);
}
 
void GPU_Adapter_RectangleRoundFilled2 (GPU_Target *target, GPU_Rect* rect, float radius, SDL_Color* color) {
  GPU_RectangleRoundFilled2(target, *rect, radius, *color);
}
 
void GPU_Adapter_Polygon (GPU_Target *target, unsigned int num_vertices, float *vertices, SDL_Color* color) {
  GPU_Polygon(target, num_vertices, vertices, *color);
}
 
void GPU_Adapter_Polyline (GPU_Target *target, unsigned int num_vertices, float *vertices, SDL_Color* color, GPU_bool close_loop) {
  GPU_Polyline(target, num_vertices, vertices, *color, close_loop);
}
 
void GPU_Adapter_PolygonFilled (GPU_Target *target, unsigned int num_vertices, float *vertices, SDL_Color* color) {
  GPU_PolygonFilled(target, num_vertices, vertices, *color);
}

// initialization
void GPU_Adapter_GetLinkedVersion(SDL_version* out) {
  *out = GPU_GetLinkedVersion();
}

GPU_Target* GPU_Adapter_InitRendererByID(GPU_RendererID* renderer_request, Uint16 w, Uint16 h, GPU_WindowFlagEnum SDL_flags) {
  return GPU_InitRendererByID(*renderer_request, w,h , SDL_flags);
}

// debugging & logging
void GPU_Adapter_PopErrorCode(GPU_ErrorObject* out) {
  *out = GPU_PopErrorCode();
}

// renderer setup

void GPU_Adapter_MakeRendererID(GPU_RendererID* out, const char* name, GPU_RendererEnum renderer, int major_version, int minor_version) {
  *out = GPU_MakeRendererID(name, renderer, major_version, minor_version);
}

void GPU_Adapter_GetRendererID(GPU_RendererID* out, GPU_RendererEnum renderer) {
  *out = GPU_GetRendererID(renderer);
}

// renderer controls
void GPU_Adapter_SetCurrentRenderer(GPU_RendererID* id) {
  GPU_SetCurrentRenderer(*id);
}

void GPU_Adapter_GetRenderer(GPU_RendererID* id) {
  GPU_GetRenderer(*id);
}

// context controls
void GPU_Adapter_GetBlendModeFromPreset(GPU_BlendMode* out, GPU_BlendPresetEnum preset) {
  *out = GPU_GetBlendModeFromPreset(preset);
}

// shader interface

void GPU_Adapter_MakeAttributeFormat(GPU_AttributeFormat* out, int num_elems_per_vertex, GPU_TypeEnum type, GPU_bool normalize, int stride_bytes, int offset_bytes) {
  *out = GPU_MakeAttributeFormat(num_elems_per_vertex, type, normalize, stride_bytes, offset_bytes);
}

void GPU_Adapter_MakeAttribute (GPU_Attribute* out, int location, void *values, GPU_AttributeFormat* format) {
  *out = GPU_MakeAttribute(location, values, *format);
}

void GPU_Adapter_LoadShaderBlock (GPU_ShaderBlock* out, Uint32 program_object, const char *position_name, const char *texcoord_name, const char *color_name, const char *modelViewMatrix_name) {
  *out = GPU_LoadShaderBlock(program_object, position_name, texcoord_name, color_name, modelViewMatrix_name);
}

void GPU_Adapter_SetShaderBlock (GPU_ShaderBlock* block) {
  GPU_SetShaderBlock(*block);
}

void GPU_Adapter_GetShaderBlock(GPU_ShaderBlock* out) {
  *out = GPU_GetShaderBlock();
}

void GPU_Adapter_SetAttributeSource(int num_values, GPU_Attribute* source) {
  GPU_SetAttributeSource(num_values, *source);
}
