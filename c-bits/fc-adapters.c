#define FC_USE_SDL_GPU
#include <SDL_FontCache.h>
#include <SDL_gpu.h>

// Object creation
void FC_Adapter_MakeRect(GPU_Rect* out, float x, float y, float w, float h) {
  *out = FC_MakeRect(x, y, w, h);
}

void FC_Adapter_MakeScale(FC_Scale* out, float x, float y) {
  *out = FC_MakeScale(x, y);
}

void FC_Adapter_MakeColor(SDL_Color* out, Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
  *out = FC_MakeColor(r, g, b, a);
}

void FC_Adapter_MakeEffect(FC_Effect* out, FC_AlignEnum alignment, FC_Scale* scale, SDL_Color* color) {
  *out = FC_MakeEffect(alignment, *scale, *color);
}

void FC_Adapter_MakeGlyphData(FC_GlyphData* out, int cache_level, Sint16 x, Sint16 y, Uint16 w, Uint16 h) {
  *out = FC_MakeGlyphData(cache_level, x, y, w, h);
}

// Font object
GPU_bool FC_Adapter_LoadFont(FC_Font* font, const char* filename_ttf, Uint32 pointSize, SDL_Color* color, int style) {
  return FC_LoadFont(font, filename_ttf, pointSize, *color, style);
}

GPU_bool FC_Adapter_LoadFontFromTTF(FC_Font* font, TTF_Font* ttf, SDL_Color* color) {
  return FC_LoadFontFromTTF(font, ttf, *color);
}

GPU_bool FC_Adapter_LoadFont_RW(FC_Font* font, SDL_RWops* file_rwops_ttf, Uint8 own_rwops, Uint32 pointSize, SDL_Color* color, int style) {
  return FC_LoadFont_RW(font, file_rwops_ttf, own_rwops, pointSize, *color, style);
}

// Rendering
void FC_Adapter_Draw(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, const char* text) {
  GPU_Rect r = FC_Draw(font, dest, x, y, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawAlign(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, FC_AlignEnum align, const char* text) {
  GPU_Rect r = FC_DrawAlign(font, dest, x, y, align, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawScale(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, FC_Scale* scale, const char* text) {
  GPU_Rect r = FC_DrawScale(font, dest, x, y, *scale, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawColor(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, SDL_Color* color, const char* text) {
  GPU_Rect r = FC_DrawColor(font, dest, x, y, *color, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawEffect(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, FC_Effect* effect, const char* text) {
  GPU_Rect r = FC_DrawEffect(font, dest, x, y, *effect, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawBox(GPU_Rect* out, FC_Font* font, GPU_Target* dest, GPU_Rect* box, const char* text) {
  GPU_Rect r = FC_DrawBox(font, dest, *box, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawBoxAlign(GPU_Rect* out, FC_Font* font, GPU_Target* dest, GPU_Rect* box, FC_AlignEnum align, const char* text) {
  GPU_Rect r = FC_DrawBoxAlign(font, dest, *box, align, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawBoxScale(GPU_Rect* out, FC_Font* font, GPU_Target* dest, GPU_Rect* box, FC_Scale* scale, const char* text) {
  GPU_Rect r = FC_DrawBoxScale(font, dest, *box, *scale, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawBoxColor(GPU_Rect* out, FC_Font* font, GPU_Target* dest, GPU_Rect* box, SDL_Color* color, const char* text) {
  GPU_Rect r = FC_DrawBoxColor(font, dest, *box, *color, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawBoxEffect(GPU_Rect* out, FC_Font* font, GPU_Target* dest, GPU_Rect* box, FC_Effect* effect, const char* text) {
  GPU_Rect r = FC_DrawBoxEffect(font, dest, *box, *effect, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawColumn(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, Uint16 width, const char* text) {
  GPU_Rect r = FC_DrawColumn(font, dest, x, y, width, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawColumnAlign(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, Uint16 width, FC_AlignEnum align, const char* text) {
  GPU_Rect r = FC_DrawColumnAlign(font, dest, x, y, width, align, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawColumnScale(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, Uint16 width, FC_Scale* scale, const char* text) {
  GPU_Rect r = FC_DrawColumnScale(font, dest, x, y, width, *scale, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawColumnColor(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, Uint16 width, SDL_Color* color, const char* text) {
  GPU_Rect r = FC_DrawColumnColor(font, dest, x, y, width, *color, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}

void FC_Adapter_DrawColumnEffect(GPU_Rect* out, FC_Font* font, GPU_Target* dest, float x, float y, Uint16 width, FC_Effect* effect, const char* text) {
  GPU_Rect r = FC_DrawColumnEffect(font, dest, x, y, width, *effect, text);
  if (out == NULL) {
    return;
  }
  *out = r;
}
