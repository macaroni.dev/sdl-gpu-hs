module Main (main) where

import System.Environment (getArgs)

import SimpleShader qualified
main :: IO ()
main = getArgs >>= \case
  ["simple-shader"] -> SimpleShader.main
  args -> error $ "Unknown args: " ++ show args
