-- A Haskell port of sdl-gpu's "simple-shader" demo:
-- https://github.com/grimfang4/sdl-gpu/blob/e8ee3522ba0dbe72ca387d978e5f49a9f31e7ba0/demos/simple-shader/main.c
module SimpleShader where

import Control.Monad (when)
import SDL.Raw qualified as SDL
import SDL.GPU.C qualified as GPU
import Foreign.Ptr
import Foreign.C.String
import Foreign.Marshal.Alloc
import Control.Monad.Managed
import Control.Exception
import Memorable

loadShader :: MonadIO m => GPU.ShaderEnum -> FilePath -> m GPU.Shader
loadShader shaderType filename = liftIO $ do
  renderer <- GPU.getCurrentRenderer
  source <- readFile filename
  header <- renderer *-> (.shader_language) >>= \lang ->
    if | lang == GPU.languageGLSL ->
         renderer *-> (.max_shader_version) >>= \v ->
           if | v >= 120 -> pure "#version 120\n"
              | otherwise -> pure "#version110\n"
       | lang == GPU.languageGLSLES -> pure "#version 100\nprecision mediump int;\nprecision mediump float;\n"
       | otherwise -> pure ""
  shader <- withCString (header ++ source) $ GPU.compileShader shaderType
  when (shader == 0) $ do
    errMsg <- GPU.getShaderMessage >>= peekCString
    error $ unwords ["Error loading shader", filename, ":", errMsg]
  pure shader

loadShaderProgram
  :: FilePath -- ^ vertex shader
  -> FilePath -- ^ fragment shader
  -> Managed (GPU.ShaderProgram, Ptr GPU.ShaderBlock)
loadShaderProgram vertexPath fragPath = do
  v <- loadShader GPU.vertexShader vertexPath -- NOT GPU.loadShader. This loadShader does some more.
  f <- loadShader GPU.fragmentShader fragPath -- NOT GPU.loadShader. This loadShader does some more.
  p <- managed $ bracket (GPU.linkShaders v f) GPU.freeShaderProgram
  when (p == 0) $ liftIO $ do
    errMsg <- GPU.getShaderMessage >>= peekCString
    error $ unwords ["Error linking shaders", show [vertexPath, fragPath], ":", errMsg]
  block <- managed $ bracket (mallocBytes $ memDataSize @GPU.ShaderBlock) free
  liftIO $ withCString "gpu_Vertex" $ \gv ->
    withCString "gpu_TexCoord" $ \gtc ->
    withCString "gpu_Color" $ \gc ->
    withCString "gpu_ModelViewProjectionMatrix" $ \gmvpm ->
    GPU.loadShaderBlock block p gv gtc gc gmvpm
  pure (p, block)

-- Not going to change dynamically, so can be done once.

prepareMaskShader :: MonadIO m => GPU.ShaderProgram -> Ptr GPU.Image -> Ptr GPU.Image -> m ()
prepareMaskShader shader image maskImage = pure ()
  
main :: IO ()
main = runManaged $ do
  image <- managed $ bracket (withCString "data/test3.png" GPU.loadImage) GPU.freeImage
  pure ()
