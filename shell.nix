(import ./default.nix).shellFor {
  tools = {
    cabal = "latest";
  };
  exactDeps = false;
  withHoogle = false;
}
